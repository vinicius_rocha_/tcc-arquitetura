import { Component, OnInit } from '@angular/core';
import { AtivosService } from '@app/_services/ativos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Ativo } from '@app/_models/ativo';
import { Cronograma } from '@app/_models/cronograma';
import { Constants } from '@app/_helpers/constants';
import { AuthenticationService } from '@app/_services';

@Component({
  selector: 'app-cronograma-detalhes',
  templateUrl: './cronograma-detalhes.component.html',
  styleUrls: ['./cronograma-detalhes.component.less']
})
export class CronogramaDetalhesComponent implements OnInit {

  const: Constants;

  pathAtivoId: Number = 0;
  pathCronogramaId: Number = 0;

  constructor(
    private ativoService: AtivosService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder : FormBuilder,
    private constants: Constants,
    private authenticationService: AuthenticationService) {
      this.const = constants;
      this.pathAtivoId = Number(this.route.snapshot.paramMap.get('ativo_id'));
      this.pathCronogramaId = Number(this.route.snapshot.paramMap.get('cronograma_id'));
  }

  private verificaPermissao() {
    let currentUser = this.authenticationService.currentUserValue;
    if(this.constants.permissoes["cronograma_detalhes"].indexOf(String(currentUser.roles[0].name)) < 0) {
      this.router.navigate(['']);
    }
  }

    cronograma: Cronograma;

    form: FormGroup;
    loading = false;
    submitted = false;


  ngOnInit() {
    this.verificaPermissao();

    this.form = this.formBuilder.group({
      id: [''],
      manutencao: ['', Validators.required],
      proxima: ['', Validators.required],
      observacoes: ['', Validators.required]
    });

      this.ativoService.getCronograma(this.pathAtivoId).pipe().subscribe(data => {
        let cId = this.pathCronogramaId;
        let c = new Cronograma();
        data.forEach(function (item, index, object) {
          if(item.id == cId) {
            c = item;
          }
        });

        this.cronograma = c;

        this.f.id.setValue(this.cronograma.id);
        this.f.manutencao.setValue(this.cronograma.tipoManutencao);
        this.f.proxima.setValue(this.cronograma.proximaManutencao);
        this.f.observacoes.setValue(this.cronograma.observacoes);
      });
  }

  get f() { return this.form.controls; }

  remover() {
    this.ativoService.removerCronograma(this.cronograma.id).pipe().subscribe(data => {
      this.loading = true;
      this.router.navigate([`/ativos-details/${this.pathAtivoId}`]);
    }, error => {
      this.loading = false;
    });
  }

  salvar() {
    this.loading = true;
    this.submitted = true;

    if(this.form.invalid) {
      this.loading = false;
      return;
    }

    this.cronograma.id = this.f.id.value;
    this.cronograma.tipoManutencao = this.f.manutencao.value;
    this.cronograma.proximaManutencao = this.f.proxima.value;
    this.cronograma.observacoes = this.f.observacoes.value;

    this.ativoService.atualizarCronograma(this.cronograma.id, this.cronograma).pipe().subscribe(data => {
      this.loading = false;
      this.submitted = false;
    }, error => {
      this.loading = false;
      this.submitted = false;
    });
  }

}
