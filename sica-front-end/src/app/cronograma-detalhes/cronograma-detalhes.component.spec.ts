import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CronogramaDetalhesComponent } from './cronograma-detalhes.component';

describe('CronogramaDetalhesComponent', () => {
  let component: CronogramaDetalhesComponent;
  let fixture: ComponentFixture<CronogramaDetalhesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CronogramaDetalhesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CronogramaDetalhesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
