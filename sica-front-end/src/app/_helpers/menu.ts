import { Injectable } from '@angular/core';

@Injectable()
export class Menu {

  menu = {
    "ADMIN" : [{
      "descricao" : "Gerenciar Usuários",
      "link" : "/users"
    }],
    "TEC" : [{
      "descricao" : "Gerenciar Ativos",
      "link" : "/ativos-list"
    }],
    "ENG" : [{
      "descricao" : "Gerenciar Ativos",
      "link" : "/ativos-list"
    },
    {
      "descricao" : "Gerenciar Tipos (Ativos)",
      "link" : "/ativo-tipo-lista"
    },
    {
      "descricao" : "Gerenciar Fornecedores",
      "link" : "/fornecedor-lista"
    },
    {
      "descricao" : "Barragens",
      "link" : "/barragem-lista"
    }],
    "CON" : [
    {
      "descricao" : "Barragens",
      "link" : "/barragem-lista"
    }]
  };

}
