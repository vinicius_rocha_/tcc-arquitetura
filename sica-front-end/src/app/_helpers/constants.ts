import { Injectable } from '@angular/core';

@Injectable()
export class Constants {

  tiposManutencao = [
    { valor: "PREVENTIVA", texto: "Preventiva" },
    { valor: "CORRETIVA", texto: "Corretiva" }
  ];

  comparadorNivelAlerta = [
    {valor: ">", texto: "Maior que"},
    {valor: ">=", texto: "Maior ou igual a"},
    {valor: "<", texto: "Menor que"},
    {valor: "<=", texto: "Menor ou igual a"},
    {valor: "=", texto: "Igual a"},
  ];

  permissoes = {
    "gerenciar_usuarios" : ["ADMIN"],
    "ativo_tipo_detalhes" : ["ENG"],
    "ativo_tipo_lista" : ["ENG"],
    "ativo_tipo_novo" : ["ENG"],
    "fornecedor_detalhes" : ["ENG"],
    "fornecedor_lista" : ["ENG"],
    "fornecedor_novo" : ["ENG"],
    "barragem_lista" : ["ENG", "CON"],
    "barragem_monitoramento" : ["ENG", "CON"],
    "ativos_lista" : ["ENG", "TEC"],
    "ativos_lista:NOVO" : ["ENG"],
    "ativos_novo" : ["ENG"],
    "ativos_detalhes" : ["ENG", "TEC"],
    "ativos_detalhes:SALVAR" : ["ENG"],
    "ativos_detalhes:REMOVER" : ["ENG"],
    "ativos_detalhes:NOVA_MANUTENCAO" : ["TEC"],
    "ativos_detalhes:NOVO_AGENDAMENTO" : ["ENG", "TEC"],
    "manutencao_novo" : ["TEC"],
    "manutencao_detalhes" : ["TEC", "ENG"],
    "manutencao_detalhes:REMOVER" : ["TEC"],
    "manutencao_detalhes:SALVAR" : ["TEC"],
    "cronograma_novo" : ["ENG", "TEC"],
    "cronograma_detalhes" : ["ENG","TEC"]
  };

}
