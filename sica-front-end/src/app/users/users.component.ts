import { Component, OnInit } from '@angular/core';
import { UserService, AuthenticationService } from '@app/_services';
import { User } from '@app/_models';
import { FormGroup, FormBuilder, Validators, FormsModule } from '@angular/forms';
import { Role } from '@app/_models/role';
import { Constants } from '@app/_helpers/constants';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.less']
})
export class UsersComponent implements OnInit {

  usernameFilter : string;

  loading = false;
  submitted = false;
  update = false;

  createUserErrorMessage : any;
  createUserMessage : any;

  userList : User[];
  typeList = [
    {"id": 1, "desc": "Administrador"},
    {"id": 2, "desc": "Técnico"},
    {"id": 3, "desc": "Engenheiro"},
    {"id": 4, "desc": "Consultor"}
  ];;

  // Form controllers
  modalTitle : string;
  userForm : FormGroup;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService : UserService,
    private formBuilder : FormBuilder,
    private constants: Constants,
    private authenticationService: AuthenticationService)
    { }

  ngOnInit() {

    this.verificaPermissao();

    this.loading = true;

    this.userForm = this.formBuilder.group({
      id: [''],
      username: ['', Validators.required],
      fullname: ['', Validators.required],
      password: ['', Validators.required],
      type: ['', Validators.required]
    });

    this.userService.getAllUsers().pipe().subscribe(allusers => {
      this.loading = false;
      this.userList = allusers;
    });
  }

  ngAfterViewInit() {
  }

  get f() { return this.userForm.controls; }

  initCreateUser() {
    this.modalTitle = "Criar novo usuário";
    this.createUserErrorMessage = null;
    this.createUserMessage = null;
    this.submitted = false;
    this.userForm.reset();
  }

  createUser() {
    this.submitted = true;
    this.createUserErrorMessage = null;
    this.createUserMessage = null;

    if(this.userForm.invalid) {
      return;
    }

    this.loading = true;

    let newUser: User = new User();
    newUser.roles = new Array<Role>();
    if(this.update)
      newUser.id = this.userForm.controls.id.value;
    newUser.username = this.userForm.controls.username.value;
    newUser.password = this.userForm.controls.password.value;
    newUser.fullname = this.userForm.controls.fullname.value;
    let userRole = new Role();
    userRole.id = this.userForm.controls.type.value;
    newUser.roles.push(userRole);

    if(!this.update) {
      this.userService.createUser(newUser).subscribe(data => {
          this.loading = false;
          this.update = false;
          this.createUserMessage = "Usuário criado com sucesso";
          this.userList.push(data);
      }, error => {
        console.log("error: " + error);
        this.loading = false;
        this.update = false;
        this.createUserErrorMessage = error;
      });
    } else {
      this.userService.update(newUser).subscribe(data => {
        this.loading = false;
        this.update = false;
        this.helperUpdateUserList(data);
        this.createUserMessage = "Usuário atualizado com sucesso";
        }, error => {
          console.log("error: " + error);
          this.loading = false;
          this.update = false;
          this.createUserErrorMessage = error;
        });
      }
  }

  deleteUser(id: Number) {
    this.userService.delete(id).subscribe(
      data => {
        this.userList.forEach(function (item, index, object) {
          if(item.id === data.id)
            object.splice(index, 1);
        });
    }, error => {

    });
  }

  initUpdateUser(selectedUser : User) {
    console.log(selectedUser);
    this.modalTitle = "Atualizar usuário";
    this.update = true;
    this.createUserErrorMessage = null;
    this.createUserMessage = null;
    this.submitted = false;
    this.userForm.reset();

    this.userForm.controls.password.setValidators([]);

    this.userForm.controls.id.setValue(selectedUser.id);
    this.userForm.controls.username.setValue(selectedUser.username);
    this.userForm.controls.password.setValue(selectedUser.password);
    this.userForm.controls.fullname.setValue(selectedUser.fullname);
    this.userForm.controls.type.setValue(selectedUser.roles[0].id);
  }

  private helperUpdateUserList(user: User) {
    this.userList.forEach(function (item, index, object) {
      if(item.id === user.id)
        object[index] = user;
    });
  }

  private verificaPermissao() {
    let currentUser = this.authenticationService.currentUserValue;
    if(this.constants.permissoes["gerenciar_usuarios"].indexOf(String(currentUser.roles[0].name)) < 0) {
      this.router.navigate(['']);
    }
  }
}
