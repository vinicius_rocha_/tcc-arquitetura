import { Component, OnInit } from '@angular/core';
import { AtivoTipo } from '@app/_models/ativo.tipo';
import { AtivosService } from '@app/_services/ativos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Constants } from '@app/_helpers/constants';
import { AuthenticationService } from '@app/_services';

@Component({
  selector: 'app-ativo-tipo-lista',
  templateUrl: './ativo-tipo-lista.component.html',
  styleUrls: ['./ativo-tipo-lista.component.less']
})
export class AtivoTipoListaComponent implements OnInit {

  constructor(
    private ativoService: AtivosService,
    private route: ActivatedRoute,
    private router: Router,
    private constants : Constants,
    private authenticationService: AuthenticationService
  ) { }

  private verificaPermissao() {
    let currentUser = this.authenticationService.currentUserValue;
    if(this.constants.permissoes["ativo_tipo_lista"].indexOf(String(currentUser.roles[0].name)) < 0) {
      this.router.navigate(['']);
    }
  }

  tipos : AtivoTipo[];
  tipoFilter: string;

  ngOnInit() {
    this.verificaPermissao();

    this.ativoService.getTipos().pipe().subscribe(data => {
      this.tipos = data;
    });
  }

}
