import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtivoTipoListaComponent } from './ativo-tipo-lista.component';

describe('AtivoTipoListaComponent', () => {
  let component: AtivoTipoListaComponent;
  let fixture: ComponentFixture<AtivoTipoListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtivoTipoListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtivoTipoListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
