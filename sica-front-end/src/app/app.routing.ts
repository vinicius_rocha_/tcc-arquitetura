﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { AuthGuard } from './_helpers';
import { UsersComponent } from './users/users.component';
import { AtivosListComponent } from './ativos-list/ativos-list.component';
import { AtivosDetaisComponent } from './ativos-detais/ativos-detais.component';
import { AtivosNovoComponent } from './ativos-novo/ativos-novo.component';
import { CronogramaDetalhesComponent } from './cronograma-detalhes/cronograma-detalhes.component';
import { CronogramaNovoComponent } from './cronograma-novo/cronograma-novo.component';
import { ManutencaoDetalhesComponent } from './manutencao-detalhes/manutencao-detalhes.component';
import { ManutencaoNovoComponent } from './manutencao-novo/manutencao-novo.component';
import { AtivoTipoListaComponent } from './ativo-tipo-lista/ativo-tipo-lista.component';
import { AtivoTipoDetalhesComponent } from './ativo-tipo-detalhes/ativo-tipo-detalhes.component';
import { AtivoTipoNovoComponent } from './ativo-tipo-novo/ativo-tipo-novo.component';
import { FornecedorListaComponent } from './fornecedor-lista/fornecedor-lista.component';
import { FornecedorDetalhesComponent } from './fornecedor-detalhes/fornecedor-detalhes.component';
import { FornecedorNovoComponent } from './fornecedor-novo/fornecedor-novo.component';
import { BarragemListaComponent } from './barragem-lista/barragem-lista.component';
import { BarragemMonitoramentoComponent } from './barragem-monitoramento/barragem-monitoramento.component';

const routes: Routes = [
    { path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'users', component: UsersComponent, canActivate: [AuthGuard] },
    { path: 'ativos-list', component: AtivosListComponent, canActivate: [AuthGuard] },
    { path: 'ativos-details/:id', component: AtivosDetaisComponent, canActivate: [AuthGuard] },
    { path: 'ativos-novo', component: AtivosNovoComponent, canActivate: [AuthGuard] },
    { path: 'cronograma-detalhes/:ativo_id/:cronograma_id', component: CronogramaDetalhesComponent, canActivate: [AuthGuard] },
    { path: 'cronograma-novo/:id', component: CronogramaNovoComponent, canActivate: [AuthGuard] },
    { path: 'manutencao-detalhes/:ativo_id/:manutencao_id', component: ManutencaoDetalhesComponent, canActivate: [AuthGuard] },
    { path: 'manutencao-novo/:id', component: ManutencaoNovoComponent, canActivate: [AuthGuard] },
    { path: 'ativo-tipo-lista', component: AtivoTipoListaComponent, canActivate: [AuthGuard] },
    { path: 'ativo-tipo-detalhes/:id', component: AtivoTipoDetalhesComponent, canActivate: [AuthGuard] },
    { path: 'ativo-tipo-novo', component: AtivoTipoNovoComponent, canActivate: [AuthGuard] },
    { path: 'fornecedor-lista', component: FornecedorListaComponent, canActivate: [AuthGuard] },
    { path: 'fornecedor-detalhes/:id', component: FornecedorDetalhesComponent, canActivate: [AuthGuard] },
    { path: 'fornecedor-novo', component: FornecedorNovoComponent, canActivate: [AuthGuard] },
    { path: 'barragem-lista', component: BarragemListaComponent, canActivate: [AuthGuard] },
    { path: 'barragem-monitoramento/:id', component: BarragemMonitoramentoComponent, canActivate: [AuthGuard] },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const appRoutingModule = RouterModule.forRoot(routes);
