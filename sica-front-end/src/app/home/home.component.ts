﻿import { Component } from '@angular/core';
import { first } from 'rxjs/operators';

import { User } from '@app/_models';
import { UserService, AuthenticationService } from '@app/_services';
import { Menu } from '@app/_helpers/menu';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {
    loading = false;
    user : User = null;
    navMenu: Menu;

    constructor(private userService: UserService, private authenticationService: AuthenticationService, private menu : Menu) {
      this.user = this.authenticationService.currentUserValue;
      this.navMenu = menu;
    }

    ngOnInit() {
    }
}
