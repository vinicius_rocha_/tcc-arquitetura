import { Component, OnInit } from '@angular/core';
import { Constants } from '@app/_helpers/constants';
import { AtivosService } from '@app/_services/ativos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Cronograma } from '@app/_models/cronograma';
import { Ativo } from '@app/_models/ativo';
import { AuthenticationService } from '@app/_services';

@Component({
  selector: 'app-cronograma-novo',
  templateUrl: './cronograma-novo.component.html',
  styleUrls: ['./cronograma-novo.component.less']
})
export class CronogramaNovoComponent implements OnInit {

  const: Constants;

  pathAtivoId: number = 0;

  constructor(
    private ativoService: AtivosService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder : FormBuilder,
    private constants: Constants,
    private authenticationService: AuthenticationService) {
      this.const = constants;
      this.pathAtivoId = Number(this.route.snapshot.paramMap.get('id'));
  }

  private verificaPermissao() {
    let currentUser = this.authenticationService.currentUserValue;
    if(this.constants.permissoes["cronograma_novo"].indexOf(String(currentUser.roles[0].name)) < 0) {
      this.router.navigate(['']);
    }
  }

    cronograma: Cronograma;

    form: FormGroup;
    loading = false;
    submitted = false;


  ngOnInit() {
    this.verificaPermissao();

    this.form = this.formBuilder.group({
      manutencao: ['', Validators.required],
      proxima: ['', Validators.required],
      observacoes: ['', Validators.required]
    });
  }

  get f() { return this.form.controls; }

  salvar() {
    this.loading = true;
    this.submitted = true;

    if(this.form.invalid) {
      this.loading = false;
      return;
    }

    this.cronograma = new Cronograma();
    this.cronograma.ativo = new Ativo();
    this.cronograma.ativo.id = this.pathAtivoId;

    this.cronograma.tipoManutencao = this.f.manutencao.value;
    this.cronograma.proximaManutencao = this.f.proxima.value;
    this.cronograma.observacoes = this.f.observacoes.value;

    this.ativoService.novoCronograma(this.cronograma).pipe().subscribe(data => {
      this.router.navigate([`/ativos-details/${this.pathAtivoId}`]);
    }, error => {
      this.loading = false;
      this.submitted = false;
    });
  }

}
