import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CronogramaNovoComponent } from './cronograma-novo.component';

describe('CronogramaNovoComponent', () => {
  let component: CronogramaNovoComponent;
  let fixture: ComponentFixture<CronogramaNovoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CronogramaNovoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CronogramaNovoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
