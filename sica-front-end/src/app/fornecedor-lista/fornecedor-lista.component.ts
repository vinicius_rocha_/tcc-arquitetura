import { Component, OnInit } from '@angular/core';
import { Fornecedor } from '@app/_models/fornecedor';
import { AtivosService } from '@app/_services/ativos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Constants } from '@app/_helpers/constants';
import { AuthenticationService } from '@app/_services';

@Component({
  selector: 'app-fornecedor-lista',
  templateUrl: './fornecedor-lista.component.html',
  styleUrls: ['./fornecedor-lista.component.less']
})
export class FornecedorListaComponent implements OnInit {

  constructor(
    private ativoService: AtivosService,
    private route: ActivatedRoute,
    private router: Router,
    private constants : Constants,
    private authenticationService: AuthenticationService
  ) { }

  private verificaPermissao() {
    let currentUser = this.authenticationService.currentUserValue;
    if(this.constants.permissoes["fornecedor_lista"].indexOf(String(currentUser.roles[0].name)) < 0) {
      this.router.navigate(['']);
    }
  }

  fornecedores : Fornecedor[];
  fornecedorFilter: string;

  ngOnInit() {
    this.verificaPermissao();

    this.ativoService.getAllFornecedor().pipe().subscribe(data => {
      this.fornecedores = data;
    });
  }

}
