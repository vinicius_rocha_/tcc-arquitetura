import { Component, OnInit } from '@angular/core';
import { AtivosService } from '@app/_services/ativos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AtivoTipo } from '@app/_models/ativo.tipo';
import { Constants } from '@app/_helpers/constants';
import { AuthenticationService } from '@app/_services';

@Component({
  selector: 'app-ativo-tipo-detalhes',
  templateUrl: './ativo-tipo-detalhes.component.html',
  styleUrls: ['./ativo-tipo-detalhes.component.less']
})
export class AtivoTipoDetalhesComponent implements OnInit {

  constructor(
    private ativoService: AtivosService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder : FormBuilder,
    private constants : Constants,
    private authenticationService: AuthenticationService)
    { }

  private verificaPermissao() {
    let currentUser = this.authenticationService.currentUserValue;
    if(this.constants.permissoes["ativo_tipo_detalhes"].indexOf(String(currentUser.roles[0].name)) < 0) {
      this.router.navigate(['']);
    }
  }

  tipo: AtivoTipo;

  form: FormGroup;
  loading = false;
  submitted = false;

  tipoId: number = 0;

  ngOnInit() {
    this.verificaPermissao();

    this.form = this.formBuilder.group({
      id: [''],
      descricao: ['', Validators.required]
    });

    this.ativoService.getTipo(Number(this.route.snapshot.paramMap.get('id'))).pipe().subscribe(data => {
      this.tipo = data;
      this.tipoId = this.tipo.id;

      this.f.id.setValue(this.tipo.id);
      this.f.descricao.setValue(this.tipo.descricao);
    });
  }

  get f() { return this.form.controls; }

  delete() {
    this.ativoService.deletarTipo(this.tipo.id).pipe().subscribe(data => {
      this.loading = true;
      this.router.navigate(['/ativo-tipo-lista']);
    });
  }

  salvar() {
    this.loading = true;
    this.submitted = true;

    if(this.form.invalid) {
      this.loading = false;
      return;
    }

    this.tipo.descricao = this.f.descricao.value;

    this.ativoService.atualizarTipo(this.tipo).pipe().subscribe(data => {
      this.loading = false;
      this.submitted = false;
    }, error => {
      this.loading = false;
      this.submitted = false;
    });
  }

}
