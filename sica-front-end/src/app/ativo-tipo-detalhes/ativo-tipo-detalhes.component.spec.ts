import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtivoTipoDetalhesComponent } from './ativo-tipo-detalhes.component';

describe('AtivoTipoDetalhesComponent', () => {
  let component: AtivoTipoDetalhesComponent;
  let fixture: ComponentFixture<AtivoTipoDetalhesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtivoTipoDetalhesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtivoTipoDetalhesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
