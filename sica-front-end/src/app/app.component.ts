﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from './_services';
import { User } from './_models';
import { Menu } from './_helpers/menu';

@Component({ selector: 'app', templateUrl: 'app.component.html' })
export class AppComponent {
    currentUser: User;
    navMenu : Menu;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private menu : Menu
    ) {
        this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
        this.navMenu = this.menu;
    }

    logout() {
        this.authenticationService.logout();
        this.router.navigate(['/login']);
    }
}
