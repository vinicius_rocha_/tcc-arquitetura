import { Component, OnInit } from '@angular/core';
import { Fornecedor } from '@app/_models/fornecedor';
import { AtivosService } from '@app/_services/ativos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Constants } from '@app/_helpers/constants';
import { AuthenticationService } from '@app/_services';

@Component({
  selector: 'app-fornecedor-detalhes',
  templateUrl: './fornecedor-detalhes.component.html',
  styleUrls: ['./fornecedor-detalhes.component.less']
})
export class FornecedorDetalhesComponent implements OnInit {

  constructor(
    private ativoService: AtivosService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder : FormBuilder,
    private constants : Constants,
    private authenticationService: AuthenticationService)
    { }

  private verificaPermissao() {
    let currentUser = this.authenticationService.currentUserValue;
    if(this.constants.permissoes["fornecedor_detalhes"].indexOf(String(currentUser.roles[0].name)) < 0) {
      this.router.navigate(['']);
    }
  }

  fornecedor: Fornecedor;

  form: FormGroup;
  loading = false;
  submitted = false;

  fornecedorId: number = 0;

  ngOnInit() {
    this.verificaPermissao();

    this.form = this.formBuilder.group({
      id: [''],
      nomefantasia: ['', Validators.required],
      cnpj: ['', Validators.required],
      endereco: ['', Validators.required],
      email: ['', Validators.required],
      tel1: ['', Validators.required],
      tel2: [''],
      tel3: ['']
    });

    this.ativoService.getFornecedor(Number(this.route.snapshot.paramMap.get('id'))).pipe().subscribe(data => {
      this.fornecedor = data;
      this.fornecedorId = this.fornecedor.id;

      this.f.id.setValue(this.fornecedor.id);
      this.f.nomefantasia.setValue(this.fornecedor.nomeFantasia);
      this.f.cnpj.setValue(this.fornecedor.cnpj);
      this.f.endereco.setValue(this.fornecedor.endereco);
      this.f.email.setValue(this.fornecedor.email);
      this.f.tel1.setValue(this.fornecedor.telefone1);
      this.f.tel2.setValue(this.fornecedor.telefone2);
      this.f.tel3.setValue(this.fornecedor.telefone3);
    });
  }

  get f() { return this.form.controls; }

  delete() {
    this.ativoService.removerFornecedor(this.fornecedor.id).pipe().subscribe(data => {
      this.loading = true;
      this.router.navigate(['/fornecedor-lista']);
    });
  }

  salvar() {
    this.loading = true;
    this.submitted = true;

    if(this.form.invalid) {
      this.loading = false;
      return;
    }

    this.fornecedor.nomeFantasia = this.f.nomefantasia.value;
    this.fornecedor.cnpj = this.f.cnpj.value;
    this.fornecedor.endereco = this.f.endereco.value;
    this.fornecedor.email = this.f.email.value;
    this.fornecedor.telefone1 = this.f.tel1.value;
    this.fornecedor.telefone2 = this.f.tel2.value;
    this.fornecedor.telefone3 = this.f.tel3.value;

    this.ativoService.atualizarFornecedor(this.fornecedor).pipe().subscribe(data => {
      this.loading = false;
      this.submitted = false;
    }, error => {
      this.loading = false;
      this.submitted = false;
    });
  }

}
