import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtivoTipoNovoComponent } from './ativo-tipo-novo.component';

describe('AtivoTipoNovoComponent', () => {
  let component: AtivoTipoNovoComponent;
  let fixture: ComponentFixture<AtivoTipoNovoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtivoTipoNovoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtivoTipoNovoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
