import { Component, OnInit } from '@angular/core';
import { AtivosService } from '@app/_services/ativos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AtivoTipo } from '@app/_models/ativo.tipo';
import { Constants } from '@app/_helpers/constants';
import { AuthenticationService } from '@app/_services';

@Component({
  selector: 'app-ativo-tipo-novo',
  templateUrl: './ativo-tipo-novo.component.html',
  styleUrls: ['./ativo-tipo-novo.component.less']
})
export class AtivoTipoNovoComponent implements OnInit {

  constructor(
    private ativoService: AtivosService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder : FormBuilder,
    private constants : Constants,
    private authenticationService: AuthenticationService)
    { }

  private verificaPermissao() {
    let currentUser = this.authenticationService.currentUserValue;
    if(this.constants.permissoes["ativo_tipo_novo"].indexOf(String(currentUser.roles[0].name)) < 0) {
      this.router.navigate(['']);
    }
  }

  tipo: AtivoTipo;

  form: FormGroup;
  loading = false;
  submitted = false;

  ngOnInit() {
    this.verificaPermissao();

    this.form = this.formBuilder.group({
      descricao: ['', Validators.required]
    });
  }

  get f() { return this.form.controls; }

  salvar() {
    this.loading = true;
    this.submitted = true;

    if(this.form.invalid) {
      this.loading = false;
      return;
    }

    this.tipo = new AtivoTipo();

    this.tipo.descricao = this.f.descricao.value;

    this.ativoService.cadastrarTipo(this.tipo).pipe().subscribe(data => {
      this.router.navigate([`/ativo-tipo-detalhes/${data.id}`]);
    }, error => {
      this.loading = false;
      this.submitted = false;
    });
  }

}
