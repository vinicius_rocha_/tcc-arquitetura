import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarragemMonitoramentoComponent } from './barragem-monitoramento.component';

describe('BarragemMonitoramentoComponent', () => {
  let component: BarragemMonitoramentoComponent;
  let fixture: ComponentFixture<BarragemMonitoramentoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarragemMonitoramentoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarragemMonitoramentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
