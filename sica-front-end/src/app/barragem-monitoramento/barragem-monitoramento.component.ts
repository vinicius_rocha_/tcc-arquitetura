import { Component, OnInit } from '@angular/core';
import { Vazao } from '@app/_models/vazao';
import { MonitoramentoService } from '@app/_services/monitoramento.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ChartDataSets, ChartType, ChartOptions } from 'chart.js';
import { Label, Color } from 'ng2-charts';
import { DatePipe } from '@angular/common';
import { Piezometro } from '@app/_models/piezometro';
import { Barragem } from '@app/_models/barragem';
import { Constants } from '@app/_helpers/constants';
import { AuthenticationService } from '@app/_services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-barragem-monitoramento',
  templateUrl: './barragem-monitoramento.component.html',
  styleUrls: ['./barragem-monitoramento.component.less']
})
export class BarragemMonitoramentoComponent implements OnInit {

  barragemId: number;
  comparadorOptions: any[];

  constructor(
    private monitoramentoService: MonitoramentoService,
    private route: ActivatedRoute,
    private router: Router,
    public datepipe: DatePipe,
    private constants : Constants,
    private authenticationService: AuthenticationService,
    private formBuilder : FormBuilder,
  )
  {
    this.barragemId = Number(this.route.snapshot.paramMap.get('id'));
    this.comparadorOptions = constants.comparadorNivelAlerta;
  }

  formVazao: FormGroup;
  formPiezometro: FormGroup;
  submitted_v = false;
  submitted_p = false;
  loading_v = false;
  loading_p = false;

  private verificaPermissao() {
    let currentUser = this.authenticationService.currentUserValue;
    if(this.constants.permissoes["barragem_monitoramento"].indexOf(String(currentUser.roles[0].name)) < 0) {
      this.router.navigate(['']);
    }
  }

  vazaoChartData: ChartDataSets[] = [{ data: [], label: 'm3/h' }];
  vazaoChartLabels: Label[] = [];
  vazaoChartOptions = {responsive: true};
  vazaoChartColors: Color[] = [{borderColor: 'rgba(15, 66, 79)', backgroundColor: 'rgba(37,92,105,0.28)'}];

  piezoChartData: ChartDataSets[] = [{ data: [], label: 'kPA' }];
  piezoChartLabels: Label[] = [];
  piezoChartOptions = {responsive: true};
  piezoChartColors: Color[] = [{borderColor: 'rgba(127, 78, 20)', backgroundColor: 'rgba(170,117,57,0.28)'}];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';

  vazaoData: Vazao[];
  piezoData: Piezometro[];

  barragem: Barragem = new Barragem();

  ngOnInit() {
    this.verificaPermissao();

    this.formVazao = this.formBuilder.group({
      comparadorVazao: ['', Validators.required],
      nivelVazao: ['', Validators.required]
    });

    this.formPiezometro = this.formBuilder.group({
      comparadorPiezometro: ['', Validators.required],
      nivelPiezometro: ['', Validators.required]
    });

    this.monitoramentoService.getVazao(this.barragemId).pipe().subscribe(data => {
      this.vazaoData = data;
      let m3 = [];
      let labels = [];

      this.vazaoData.forEach(function (item, index, object) {
        m3.push(item.metrosCubicos);
        let fData = item.data.toString().split('T')[0];
        labels.push(fData);
      });

      this.vazaoChartLabels = labels;
      this.vazaoChartData[0].data = m3;
    });

    this.monitoramentoService.getPiezometro(this.barragemId).pipe().subscribe(data => {
      this.piezoData = data;
      let kpa = [];
      let labels = [];

      this.piezoData.forEach(function (item, index, object) {
        kpa.push(item.kpa);
        let fData = item.data.toString().split('T')[0];
        labels.push(fData);
      });

      this.piezoChartLabels = labels;
      this.piezoChartData[0].data = kpa;
    });

    this.monitoramentoService.getBarragem(this.barragemId).pipe().subscribe(data => {
      this.barragem = data;

      if(this.barragem.nivelAlertaVazao && this.barragem.nivelAlertaVazao.toString().split(",").length == 2) {
        let opcaoVazao = this.barragem.nivelAlertaVazao.toString().split(",")[0];
        let nivelVazao = this.barragem.nivelAlertaVazao.toString().split(",")[1];

        this.fv.comparadorVazao.setValue(opcaoVazao);
        this.fv.nivelVazao.setValue(nivelVazao);
      }

      if(this.barragem.nivelAlertaPiezometro && this.barragem.nivelAlertaPiezometro.toString().split(",").length == 2) {
        let opcaoPiezometro = this.barragem.nivelAlertaPiezometro.toString().split(",")[0];
        let nivelPiezometro = this.barragem.nivelAlertaPiezometro.toString().split(",")[1];

        this.fp.comparadorPiezometro.setValue(opcaoPiezometro);
        this.fp.nivelPiezometro.setValue(nivelPiezometro);
      }
    });
  }

  alertaAcionado = false;
  alerta() {
    //this.monitoramentoService.alerta(this.barragemId).pipe().subscribe(data => {
      this.alertaAcionado = true;
    //});
  }

  get fv() { return this.formVazao.controls; }
  get fp() { return this.formPiezometro.controls; }

  salvarNivelVazao() {
    this.loading_v = true;
    this.submitted_v = true;

    if(this.formVazao.invalid) {
      this.loading_v = false;
      return;
    }

    this.barragem.nivelAlertaVazao = this.fv.comparadorVazao.value + "," + this.fv.nivelVazao.value;

    this.monitoramentoService.updateBarragem(this.barragemId, this.barragem).pipe().subscribe(data => {
      this.loading_v = false;
      this.submitted_v = false;
    });
  }

  salvarNivelPiezometro() {
    this.loading_p = true;
    this.submitted_p = true;

    if(this.formPiezometro.invalid) {
      this.loading_p = false;
      return;
    }

    this.barragem.nivelAlertaPiezometro = this.fp.comparadorPiezometro.value + "," + this.fp.nivelPiezometro.value;

    this.monitoramentoService.updateBarragem(this.barragemId, this.barragem).pipe().subscribe(data => {
      this.loading_p = false;
      this.submitted_p = false;
    });
  }

}
