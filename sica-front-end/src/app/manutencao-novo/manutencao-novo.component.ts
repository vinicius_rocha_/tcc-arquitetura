import { Component, OnInit } from '@angular/core';
import { Constants } from '@app/_helpers/constants';
import { AtivosService } from '@app/_services/ativos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Manutencao } from '@app/_models/manutencao';
import { AuthenticationService } from '@app/_services/authentication.service';
import { User } from '@app/_models';
import { Observable } from 'rxjs';
import { Ativo } from '@app/_models/ativo';

@Component({
  selector: 'app-manutencao-novo',
  templateUrl: './manutencao-novo.component.html',
  styleUrls: ['./manutencao-novo.component.less']
})
export class ManutencaoNovoComponent implements OnInit {

  const: Constants;
  currentUser: User;

  pathAtivoId: number = 0;

  constructor(
    private ativoService: AtivosService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder : FormBuilder,
    private constants: Constants,
    private authenticationService: AuthenticationService) {
      this.const = constants;
      this.pathAtivoId = Number(this.route.snapshot.paramMap.get('id'));

      authenticationService.currentUser.subscribe(user => { this.currentUser = user });
  }

  private verificaPermissao() {
    let currentUser = this.authenticationService.currentUserValue;
    if(this.constants.permissoes["manutencao_novo"].indexOf(String(currentUser.roles[0].name)) < 0) {
      this.router.navigate(['']);
    }
  }

    manutencao: Manutencao;

    form: FormGroup;
    loading = false;
    submitted = false;


  ngOnInit() {
    this.verificaPermissao();

    this.form = this.formBuilder.group({
      usuario: ['', Validators.required],
      datarealizada: ['', Validators.required],
      observacoes: ['', Validators.required],
      manutencao: ['', Validators.required]
    });

    this.f.usuario.setValue(this.currentUser.fullname);
  }

  get f() { return this.form.controls; }

  salvar() {
    this.loading = true;
    this.submitted = true;

    if(this.form.invalid) {
      this.loading = false;
      return;
    }

    this.manutencao = new Manutencao();
    this.manutencao.ativo = new Ativo();
    this.manutencao.ativo.id = this.pathAtivoId;

    this.manutencao.usuario = this.f.usuario.value;
    this.manutencao.dataRealizada = this.f.datarealizada.value;
    this.manutencao.observacoes = this.f.observacoes.value;
    this.manutencao.tipoManutencao = this.f.manutencao.value;

    this.ativoService.novoManutencao(this.manutencao).pipe().subscribe(data => {
      this.router.navigate([`/ativos-details/${this.pathAtivoId}`]);
    }, error => {
      this.loading = false;
      this.submitted = false;
    });
  }

}
