import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManutencaoNovoComponent } from './manutencao-novo.component';

describe('ManutencaoNovoComponent', () => {
  let component: ManutencaoNovoComponent;
  let fixture: ComponentFixture<ManutencaoNovoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManutencaoNovoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManutencaoNovoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
