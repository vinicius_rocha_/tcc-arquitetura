import { Component, OnInit } from '@angular/core';
import { MonitoramentoService } from '@app/_services/monitoramento.service';
import { Barragem } from '@app/_models/barragem';
import { ActivatedRoute, Router } from '@angular/router';
import { Constants } from '@app/_helpers/constants';
import { AuthenticationService } from '@app/_services';

@Component({
  selector: 'app-barragem-lista',
  templateUrl: './barragem-lista.component.html',
  styleUrls: ['./barragem-lista.component.less']
})
export class BarragemListaComponent implements OnInit {

  constructor(
    private monitoramentoService: MonitoramentoService,
    private route: ActivatedRoute,
    private router: Router,
    private constants : Constants,
    private authenticationService: AuthenticationService
  ) { }

  private verificaPermissao() {
    let currentUser = this.authenticationService.currentUserValue;
    if(this.constants.permissoes["barragem_lista"].indexOf(String(currentUser.roles[0].name)) < 0) {
      this.router.navigate(['']);
    }
  }

  barragens : Barragem[];
  barragemFilter: string;

  ngOnInit() {
    this.verificaPermissao();

    this.monitoramentoService.getBarragens().pipe().subscribe(data => {
      this.barragens = data;
    }, error => {

    });
  }

}
