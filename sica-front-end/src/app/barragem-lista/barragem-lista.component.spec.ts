import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarragemListaComponent } from './barragem-lista.component';

describe('BarragemListaComponent', () => {
  let component: BarragemListaComponent;
  let fixture: ComponentFixture<BarragemListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarragemListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarragemListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
