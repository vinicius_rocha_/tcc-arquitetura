﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// used to create fake backend
import { fakeBackendProvider } from './_helpers';

import { AppComponent } from './app.component';
import { appRoutingModule } from './app.routing';

import { ChartsModule } from 'ng2-charts';

import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { Menu } from './_helpers/menu';;
import { UsersComponent } from './users/users.component'
import { FilterPipe } from './users/filter.pipe';;
import { AtivosListComponent } from './ativos-list/ativos-list.component';
import { AtivosDetaisComponent } from './ativos-detais/ativos-detais.component';
import { AtivosNovoComponent } from './ativos-novo/ativos-novo.component';;
import { CronogramaDetalhesComponent } from './cronograma-detalhes/cronograma-detalhes.component';
import { CronogramaNovoComponent } from './cronograma-novo/cronograma-novo.component';
import { ManutencaoDetalhesComponent } from './manutencao-detalhes/manutencao-detalhes.component';
import { ManutencaoNovoComponent } from './manutencao-novo/manutencao-novo.component';
import { Constants } from './_helpers/constants';
import { FornecedorListaComponent } from './fornecedor-lista/fornecedor-lista.component';
import { FornecedorDetalhesComponent } from './fornecedor-detalhes/fornecedor-detalhes.component';
import { FornecedorNovoComponent } from './fornecedor-novo/fornecedor-novo.component';
import { AtivoTipoListaComponent } from './ativo-tipo-lista/ativo-tipo-lista.component';
import { AtivoTipoDetalhesComponent } from './ativo-tipo-detalhes/ativo-tipo-detalhes.component';
import { AtivoTipoNovoComponent } from './ativo-tipo-novo/ativo-tipo-novo.component';
import { BarragemListaComponent } from './barragem-lista/barragem-lista.component';
import { BarragemMonitoramentoComponent } from './barragem-monitoramento/barragem-monitoramento.component';
import { DatePipe } from '@angular/common';

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        appRoutingModule,
        FormsModule,
        ChartsModule
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        LoginComponent,
        UsersComponent,
        FilterPipe,
        AtivosListComponent,
        AtivosDetaisComponent,
        AtivosNovoComponent,
        CronogramaDetalhesComponent ,
        CronogramaNovoComponent ,
        ManutencaoDetalhesComponent ,
        ManutencaoNovoComponent ,
        FornecedorListaComponent ,
        FornecedorDetalhesComponent ,
        FornecedorNovoComponent,
        AtivoTipoNovoComponent,
        AtivoTipoDetalhesComponent,
        AtivoTipoListaComponent
,
        BarragemListaComponent ,
        BarragemMonitoramentoComponent     ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

        // provider used to create fake backend
        fakeBackendProvider,
        Menu,
        Constants,
        DatePipe
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
