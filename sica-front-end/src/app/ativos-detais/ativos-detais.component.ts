import { Component, OnInit } from '@angular/core';
import { AtivosService } from '@app/_services/ativos.service';
import { Ativo } from '@app/_models/ativo';
import { ActivatedRoute, Router } from '@angular/router';
import { AtivoTipo } from '@app/_models/ativo.tipo';
import { Fornecedor } from '@app/_models/fornecedor';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Cronograma } from '@app/_models/cronograma';
import { Manutencao } from '@app/_models/manutencao';
import { Constants } from '@app/_helpers/constants';
import { AuthenticationService } from '@app/_services';

@Component({
  selector: 'app-ativos-detais',
  templateUrl: './ativos-detais.component.html',
  styleUrls: ['./ativos-detais.component.less']
})
export class AtivosDetaisComponent implements OnInit {

  constructor(
    private ativoService: AtivosService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder : FormBuilder,
    private constants : Constants,
    private authenticationService: AuthenticationService) {

  }

  exibeBotaoSalvar = true;
  exibeBotaoRemover = true;
  exibeBotaoNovoAgendamento = true;
  exibeBotaoNovaManutencao = true;

  private verificaPermissao() {
    let currentUser = this.authenticationService.currentUserValue;
    if(this.constants.permissoes["ativos_detalhes"].indexOf(String(currentUser.roles[0].name)) < 0) {
      this.router.navigate(['']);
    }

    if(this.constants.permissoes["ativos_detalhes:SALVAR"].indexOf(String(currentUser.roles[0].name)) < 0) {
      this.exibeBotaoSalvar = false;
    }

    if(this.constants.permissoes["ativos_detalhes:REMOVER"].indexOf(String(currentUser.roles[0].name)) < 0) {
      this.exibeBotaoRemover = false;
    }

    if(this.constants.permissoes["ativos_detalhes:NOVA_MANUTENCAO"].indexOf(String(currentUser.roles[0].name)) < 0) {
      this.exibeBotaoNovaManutencao = false;
    }

    if(this.constants.permissoes["ativos_detalhes:NOVO_AGENDAMENTO"].indexOf(String(currentUser.roles[0].name)) < 0) {
      this.exibeBotaoNovoAgendamento = false;
    }
  }

  ativo: Ativo;
  cronogramas: Cronograma[];
  manutencoes: Manutencao[];

  tipos: AtivoTipo[] = null;
  fornecedores: Fornecedor[] = null;
  form: FormGroup;
  loading = false;
  submitted = false;

  ativoId: number = 0;

  ngOnInit() {
    this.verificaPermissao();

    this.form = this.formBuilder.group({
      id: [''],
      descricao: ['', Validators.required],
      tipo: ['', Validators.required],
      nota: ['', Validators.required],
      aquisicao: ['', Validators.required],
      fornecedor: ['', Validators.required]
    });

    this.ativoService.getAtivo(Number(this.route.snapshot.paramMap.get('id'))).pipe().subscribe(data => {
      this.ativo = data;
      this.ativoId = this.ativo.id;

      this.ativoService.getCronograma(this.ativo.id).pipe().subscribe(data => {
        this.cronogramas = data;
      });

      this.ativoService.getManutencao(this.ativo.id).pipe().subscribe(data => {
        this.manutencoes = data;
      });

      this.f.id.setValue(this.ativo.id);
      this.f.descricao.setValue(this.ativo.descricao);
      this.f.tipo.setValue(this.ativo.tipo.id);
      this.f.nota.setValue(this.ativo.notaFiscal);
      this.f.aquisicao.setValue(this.ativo.dataAquisicao);
      this.f.fornecedor.setValue(this.ativo.fornecedor.id);
    });

    this.ativoService.getTipos().pipe().subscribe(data => {
      this.tipos = data
    });

    this.ativoService.getAllFornecedor().pipe().subscribe(data => {
      this.fornecedores = data;
    });
  }

  get f() { return this.form.controls; }

  delete() {
    this.ativoService.delete(this.ativo.id).pipe().subscribe(data => {
      this.loading = true;
      this.router.navigate(['/ativos-list']);
    });
  }

  salvar() {
    this.loading = true;
    this.submitted = true;

    if(this.form.invalid) {
      this.loading = false;
      return;
    }

    this.ativo.descricao = this.f.descricao.value;
    this.ativo.tipo.id = this.f.tipo.value;
    this.ativo.notaFiscal = this.f.nota.value;
    this.ativo.dataAquisicao = this.f.aquisicao.value;
    this.ativo.fornecedor.id = this.f.fornecedor.value;

    this.ativoService.update(this.ativo, this.ativo.id).pipe().subscribe(data => {
      this.loading = false;
      this.submitted = false;
    }, error => {
      this.loading = false;
      this.submitted = false;
    });
  }

}
