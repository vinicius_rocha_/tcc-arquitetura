import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtivosDetaisComponent } from './ativos-detais.component';

describe('AtivosDetaisComponent', () => {
  let component: AtivosDetaisComponent;
  let fixture: ComponentFixture<AtivosDetaisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtivosDetaisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtivosDetaisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
