import { Ativo } from './ativo';

export class Cronograma {
  id?: number;
  tipoManutencao: string;
  proximaManutencao: Date;
  observacoes: string;
  ativo?: Ativo;
}
