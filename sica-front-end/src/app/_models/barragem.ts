export class Barragem {
  id?: Number;
  nome: string;
  municipio: string;
  estado: string;
  latitude: string;
  longitude: string;
  nivelAlertaVazao: string;
  nivelAlertaPiezometro: string;
  bacia: string;
}
