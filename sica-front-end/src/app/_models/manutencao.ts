import { Ativo } from './ativo';

export class Manutencao {
  id: Number;
  usuario: string;
  dataRealizada: Date;
  observacoes: string;
  tipoManutencao: string;
  ativo?: Ativo;
}
