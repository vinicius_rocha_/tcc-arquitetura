import { Fornecedor } from './fornecedor';
import { AtivoTipo } from './ativo.tipo';

export class Ativo {
  id?: number;
  descricao: string;
  tipo: AtivoTipo;
  notaFiscal: string;
  dataAquisicao: Date;
  fornecedor: Fornecedor;
}
