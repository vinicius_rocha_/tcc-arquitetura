export class Fornecedor {
  id?: number;
  nomeFantasia: string;
  cnpj: string;
  endereco: string;
  email: string;
  telefone1: string;
  telefone2?: string;
  telefone3?: string;
}
