﻿import { Role } from './role';

export class User {
    id: number;
    username: string;
    fullname: string;
    password?: string;
    token?: string;
    roles?: Role[];
}
