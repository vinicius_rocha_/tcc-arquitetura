import { Component, OnInit } from '@angular/core';
import { AtivosService } from '@app/_services/ativos.service';
import { Ativo } from '@app/_models/ativo';
import { Constants } from '@app/_helpers/constants';
import { AuthenticationService } from '@app/_services';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-ativos-list',
  templateUrl: './ativos-list.component.html',
  styleUrls: ['./ativos-list.component.less']
})
export class AtivosListComponent implements OnInit {

  constructor(
    private ativoService: AtivosService,
    private constants : Constants,
    private authenticationService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  exibeBotaoNovo = true;

  private verificaPermissao() {
    let currentUser = this.authenticationService.currentUserValue;
    if(this.constants.permissoes["ativos_lista"].indexOf(String(currentUser.roles[0].name)) < 0) {
      this.router.navigate(['']);
    }

    if(this.constants.permissoes["ativos_lista:NOVO"].indexOf(String(currentUser.roles[0].name)) < 0) {
      this.exibeBotaoNovo = false;
    }
  }

  ativoList : Ativo[];
  ativoFilter: string;

  ngOnInit() {
    this.verificaPermissao();

    this.ativoService.getAllAtivos().pipe().subscribe(data => {
      this.ativoList = data;
    });
  }
}
