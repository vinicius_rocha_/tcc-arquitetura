import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtivosNovoComponent } from './ativos-novo.component';

describe('AtivosNovoComponent', () => {
  let component: AtivosNovoComponent;
  let fixture: ComponentFixture<AtivosNovoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtivosNovoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtivosNovoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
