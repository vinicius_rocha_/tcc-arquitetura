import { Component, OnInit } from '@angular/core';
import { AtivoTipo } from '@app/_models/ativo.tipo';
import { Fornecedor } from '@app/_models/fornecedor';
import { Ativo } from '@app/_models/ativo';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AtivosService } from '@app/_services/ativos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Constants } from '@app/_helpers/constants';
import { AuthenticationService } from '@app/_services';

@Component({
  selector: 'app-ativos-novo',
  templateUrl: './ativos-novo.component.html',
  styleUrls: ['./ativos-novo.component.less']
})
export class AtivosNovoComponent implements OnInit {

  constructor(
    private ativoService: AtivosService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder : FormBuilder,
    private constants : Constants,
    private authenticationService: AuthenticationService)
    { }

  private verificaPermissao() {
    let currentUser = this.authenticationService.currentUserValue;
    if(this.constants.permissoes["ativos_novo"].indexOf(String(currentUser.roles[0].name)) < 0) {
      this.router.navigate(['']);
    }
  }

  ativo: Ativo;

  tipos: AtivoTipo[] = null;
  fornecedores: Fornecedor[] = null;
  form: FormGroup;
  loading = false;
  submitted = false;

  ngOnInit() {
    this.verificaPermissao();

    this.form = this.formBuilder.group({
      id: [''],
      descricao: ['', Validators.required],
      tipo: ['', Validators.required],
      nota: ['', Validators.required],
      aquisicao: ['', Validators.required],
      fornecedor: ['', Validators.required]
    });

    this.ativoService.getTipos().pipe().subscribe(data => {
      this.tipos = data
    });

    this.ativoService.getAllFornecedor().pipe().subscribe(data => {
      this.fornecedores = data;
    });
  }

  get f() { return this.form.controls; }

  salvar() {
    this.loading = true;
    this.submitted = true;

    if(this.form.invalid) {
      this.loading = false;
      return;
    }

    this.ativo = new Ativo();
    this.ativo.tipo = new AtivoTipo();
    this.ativo.fornecedor = new Fornecedor();

    this.ativo.descricao = this.f.descricao.value;
    this.ativo.tipo.id = this.f.tipo.value;
    this.ativo.notaFiscal = this.f.nota.value;
    this.ativo.dataAquisicao = this.f.aquisicao.value;
    this.ativo.fornecedor.id = this.f.fornecedor.value;

    this.ativoService.create(this.ativo).pipe().subscribe(data => {
      this.router.navigate([`/ativos-details/${data.id}`]);
    }, error => {
      this.loading = false;
      this.submitted = false;
    });
  }

}
