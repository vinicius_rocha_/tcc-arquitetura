import { Component, OnInit } from '@angular/core';
import { AtivosService } from '@app/_services/ativos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Fornecedor } from '@app/_models/fornecedor';
import { Constants } from '@app/_helpers/constants';
import { AuthenticationService } from '@app/_services';

@Component({
  selector: 'app-fornecedor-novo',
  templateUrl: './fornecedor-novo.component.html',
  styleUrls: ['./fornecedor-novo.component.less']
})
export class FornecedorNovoComponent implements OnInit {

  constructor(
    private ativoService: AtivosService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder : FormBuilder,
    private constants : Constants,
    private authenticationService: AuthenticationService)
    { }

  private verificaPermissao() {
    let currentUser = this.authenticationService.currentUserValue;
    if(this.constants.permissoes["fornecedor_novo"].indexOf(String(currentUser.roles[0].name)) < 0) {
      this.router.navigate(['']);
    }
  }

  fornecedor: Fornecedor;

  form: FormGroup;
  loading = false;
  submitted = false;

  ngOnInit() {
    this.verificaPermissao();

    this.form = this.formBuilder.group({
      nomefantasia: ['', Validators.required],
      cnpj: ['', Validators.required],
      endereco: ['', Validators.required],
      email: ['', Validators.required],
      tel1: ['', Validators.required],
      tel2: [''],
      tel3: ['']
    });
  }

  get f() { return this.form.controls; }

  salvar() {
    this.loading = true;
    this.submitted = true;

    if(this.form.invalid) {
      this.loading = false;
      return;
    }

    this.fornecedor = new Fornecedor();

    this.fornecedor.nomeFantasia = this.f.nomefantasia.value;
    this.fornecedor.cnpj = this.f.cnpj.value;
    this.fornecedor.endereco = this.f.endereco.value;
    this.fornecedor.email = this.f.email.value;
    this.fornecedor.telefone1 = this.f.tel1.value;
    this.fornecedor.telefone2 = this.f.tel2.value;
    this.fornecedor.telefone3 = this.f.tel3.value;

    this.ativoService.cadastrarFornecedor(this.fornecedor).pipe().subscribe(data => {
      this.router.navigate([`/fornecedor-detalhes/${data.id}`]);
    }, error => {
      this.loading = false;
      this.submitted = false;
    });
  }

}
