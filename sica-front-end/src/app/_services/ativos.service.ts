import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Ativo } from '@app/_models/ativo';
import { Fornecedor } from '@app/_models/fornecedor';
import { AtivoTipo } from '@app/_models/ativo.tipo';
import { environment } from '@environments/environment';
import { Cronograma } from '@app/_models/cronograma';
import { Manutencao } from '@app/_models/manutencao';

@Injectable({
  providedIn: 'root'
})
export class AtivosService {

  constructor(private http: HttpClient) { }

  getAllAtivos() {
    return this.http.get<Ativo[]>(`${environment.apiUrl}/${environment.backendAtivo}/`);
  }

  getAtivo(id: Number) {
    return this.http.get<Ativo>(`${environment.apiUrl}/${environment.backendAtivo}/${id}`);
  }

  update(ativo: Ativo, id: Number) {
    return this.http.put<Ativo>(`${environment.apiUrl}/${environment.backendAtivo}/${id}`, ativo);
  }

  create(ativo: Ativo) {
    return this.http.post<Ativo>(`${environment.apiUrl}/${environment.backendAtivo}/`, ativo);
  }

  delete(id: Number) {
    return this.http.delete<any>(`${environment.apiUrl}/${environment.backendAtivo}/${id}`);
  }

  getAllFornecedor() {
    return this.http.get<Fornecedor[]>(`${environment.apiUrl}/${environment.backendAtivo}/fornecedor`);
  }

  getFornecedor(id: Number) {
    return this.http.get<Fornecedor>(`${environment.apiUrl}/${environment.backendAtivo}/fornecedor/${id}`);
  }

  getTipos() {
    return this.http.get<AtivoTipo[]>(`${environment.apiUrl}/${environment.backendAtivo}/tipos`);
  }

  getCronograma(id: Number) {
    return this.http.get<Cronograma[]>(`${environment.apiUrl}/${environment.backendAtivo}/cronograma/ativo/${id}`);
  }

  getCronogramaTodos() {
    return this.http.get<Cronograma[]>(`${environment.apiUrl}/${environment.backendAtivo}/cronograma/`);
  }

  novoCronograma(cronograma: Cronograma) {
    return this.http.post<Cronograma[]>(`${environment.apiUrl}/${environment.backendAtivo}/cronograma/`, cronograma);
  }

  atualizarCronograma(id: Number, cronograma: Cronograma) {
    return this.http.put<Cronograma[]>(`${environment.apiUrl}/${environment.backendAtivo}/cronograma/${id}`, cronograma);
  }

  removerCronograma(id: Number) {
    return this.http.delete<Cronograma[]>(`${environment.apiUrl}/${environment.backendAtivo}/cronograma/${id}`);
  }

  getManutencao(id: Number) {
    return this.http.get<Manutencao[]>(`${environment.apiUrl}/${environment.backendAtivo}/manutencao/ativo/${id}`);
  }

  getManutencaoTodos() {
    return this.http.get<Manutencao[]>(`${environment.apiUrl}/${environment.backendAtivo}/manutencao/`);
  }

  novoManutencao(manutencao: Manutencao) {
    return this.http.post<Manutencao[]>(`${environment.apiUrl}/${environment.backendAtivo}/manutencao/`, manutencao);
  }

  atualizarManutencao(id: Number, manutencao: Manutencao) {
    return this.http.put<Manutencao[]>(`${environment.apiUrl}/${environment.backendAtivo}/manutencao/${id}`, manutencao);
  }

  removerManutencao(id: Number) {
    return this.http.delete<Manutencao[]>(`${environment.apiUrl}/${environment.backendAtivo}/manutencao/${id}`);
  }

  getTipo(id: number) {
    return this.http.get<AtivoTipo>(`${environment.apiUrl}/${environment.backendAtivo}/tipos/${id}`);
  }

  cadastrarTipo(tipo: AtivoTipo) {
    return this.http.post<AtivoTipo>(`${environment.apiUrl}/${environment.backendAtivo}/tipos`, tipo);
  }

  atualizarTipo(tipo: AtivoTipo) {
    return this.http.put<AtivoTipo>(`${environment.apiUrl}/${environment.backendAtivo}/tipos`, tipo);
  }

  deletarTipo(id: number) {
    return this.http.delete<AtivoTipo>(`${environment.apiUrl}/${environment.backendAtivo}/tipos/${id}`);
  }

  cadastrarFornecedor(fornecedor: Fornecedor) {
    return this.http.post<Fornecedor>(`${environment.apiUrl}/${environment.backendAtivo}/fornecedor/`, fornecedor);
  }

  atualizarFornecedor(fornecedor: Fornecedor) {
    return this.http.put<Fornecedor>(`${environment.apiUrl}/${environment.backendAtivo}/fornecedor/`, fornecedor);
  }

  removerFornecedor(id: number) {
    return this.http.delete<Fornecedor>(`${environment.apiUrl}/${environment.backendAtivo}/fornecedor/${id}`);
  }
}
