﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '@environments/environment';
import { User } from '@app/_models';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getUserInfo(id: Number) {
        return this.http.get<User>(`${environment.apiUrl}/${environment.backendAdmin}/user/${id}`);
    }

    getAllUsers() {
      return this.http.get<User[]>(`${environment.apiUrl}/${environment.backendAdmin}/user/all`);
    }

    createUser(user: User) {
      return this.http.post<User>(`${environment.apiUrl}/${environment.backendAdmin}/user`, user);
    }ß

    update(user: User) {
      return this.http.put<User>(`${environment.apiUrl}/${environment.backendAdmin}/user`, user);
    }

    delete(id: Number) {
      return this.http.delete<User>(`${environment.apiUrl}/${environment.backendAdmin}/user/${id}`);
    }
}
