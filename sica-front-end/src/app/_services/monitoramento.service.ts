import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Barragem } from '@app/_models/barragem';
import { environment } from '@environments/environment';
import { Vazao } from '@app/_models/vazao';
import { Piezometro } from '@app/_models/piezometro';

@Injectable({
  providedIn: 'root'
})
export class MonitoramentoService {

  constructor(private http: HttpClient) { }

  fix: Barragem = new Barragem();

  getBarragens() {
    return this.http.get<Barragem[]>(`${environment.apiUrl}/${environment.backendMonitoramento}/barragem`);
  }

  getBarragem(id: number) {
    return this.http.get<Barragem>(`${environment.apiUrl}/${environment.backendMonitoramento}/barragem/${id}`);
  }

  getPiezometro(id: number) {
    return this.http.get<Piezometro[]>(`${environment.apiUrl}/${environment.backendMonitoramento}/barragem/${id}/piezometro`);
  }

  getVazao(id: number) {
    return this.http.get<Vazao[]>(`${environment.apiUrl}/${environment.backendMonitoramento}/barragem/${id}/vazao`);
  }

  alerta(id: number) {
    return this.http.post<Barragem>(`${environment.apiUrl}/comunicacao/barragem/${id}`, this.fix);
  }

  updateBarragem(id: number, barragem: Barragem) {
    return this.http.post<Barragem>(`${environment.apiUrl}/${environment.backendMonitoramento}/barragem/${id}`, barragem);
  }
}
