import { TestBed } from '@angular/core/testing';

import { AtivosService } from './ativos.service';

describe('AtivosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AtivosService = TestBed.get(AtivosService);
    expect(service).toBeTruthy();
  });
});
