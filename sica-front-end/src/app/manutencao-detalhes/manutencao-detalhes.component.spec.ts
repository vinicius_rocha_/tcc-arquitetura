import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManutencaoDetalhesComponent } from './manutencao-detalhes.component';

describe('ManutencaoDetalhesComponent', () => {
  let component: ManutencaoDetalhesComponent;
  let fixture: ComponentFixture<ManutencaoDetalhesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManutencaoDetalhesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManutencaoDetalhesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
