import { Component, OnInit } from '@angular/core';
import { Constants } from '@app/_helpers/constants';
import { AtivosService } from '@app/_services/ativos.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Cronograma } from '@app/_models/cronograma';
import { Manutencao } from '@app/_models/manutencao';
import { AuthenticationService } from '@app/_services';

@Component({
  selector: 'app-manutencao-detalhes',
  templateUrl: './manutencao-detalhes.component.html',
  styleUrls: ['./manutencao-detalhes.component.less']
})
export class ManutencaoDetalhesComponent implements OnInit {

  const: Constants;

  pathAtivoId: Number = 0;
  pathManutencaoId: Number = 0;

  constructor(
    private ativoService: AtivosService,
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder : FormBuilder,
    private constants: Constants,
    private authenticationService: AuthenticationService) {
      this.const = constants;
      this.pathAtivoId = Number(this.route.snapshot.paramMap.get('ativo_id'));
      this.pathManutencaoId = Number(this.route.snapshot.paramMap.get('manutencao_id'));
    }

    exibeBotaoSalvar = true;
    exibeBotaoRemover = true;

    private verificaPermissao() {
      let currentUser = this.authenticationService.currentUserValue;
      if(this.const.permissoes["manutencao_detalhes"].indexOf(String(currentUser.roles[0].name)) < 0) {
        this.router.navigate(['']);
      }

      if(this.const.permissoes["manutencao_detalhes:REMOVER"].indexOf(String(currentUser.roles[0].name)) < 0) {
        this.exibeBotaoRemover = false;
      }

      if(this.const.permissoes["manutencao_detalhes:SALVAR"].indexOf(String(currentUser.roles[0].name)) < 0) {
        this.exibeBotaoSalvar = false;
      }
    }

    manutencao: Manutencao;

    form: FormGroup;
    loading = false;
    submitted = false;


  ngOnInit() {
    this.verificaPermissao();

    this.form = this.formBuilder.group({
      id: [''],
      usuario: ['', Validators.required],
      datarealizada: ['', Validators.required],
      observacoes: ['', Validators.required],
      manutencao: ['', Validators.required]
    });

      this.ativoService.getManutencao(this.pathAtivoId).pipe().subscribe(data => {
        let mId = this.pathManutencaoId;
        let m = new Manutencao();
        data.forEach(function (item, index, object) {
          if(item.id == mId) {
            m = item;
          }
        });

        this.manutencao = m;

        this.f.id.setValue(this.manutencao.id);
        this.f.usuario.setValue(this.manutencao.usuario);
        this.f.datarealizada.setValue(this.manutencao.dataRealizada);
        this.f.observacoes.setValue(this.manutencao.observacoes);
        this.f.manutencao.setValue(this.manutencao.tipoManutencao);
      });
  }

  get f() { return this.form.controls; }

  remover() {
    this.ativoService.removerManutencao(this.manutencao.id).pipe().subscribe(data => {
      this.loading = true;
      this.router.navigate([`/ativos-details/${this.pathAtivoId}`]);
    }, error => {
      this.loading = false;
    });
  }

  salvar() {
    this.loading = true;
    this.submitted = true;

    if(this.form.invalid) {
      this.loading = false;
      return;
    }

    this.manutencao.id = this.f.id.value;
    this.manutencao.usuario = this.f.usuario.value;
    this.manutencao.dataRealizada = this.f.datarealizada.value;
    this.manutencao.observacoes = this.f.observacoes.value;
    this.manutencao.tipoManutencao = this.f.manutencao.value;

    this.ativoService.atualizarManutencao(this.manutencao.id, this.manutencao).pipe().subscribe(data => {
      this.loading = false;
      this.submitted = false;
    }, error => {
      this.loading = false;
      this.submitted = false;
    });
  }

}
