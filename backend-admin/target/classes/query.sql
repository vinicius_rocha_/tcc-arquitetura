INSERT INTO role (id, description, name) VALUES (1, 'Administrador', 'ADMIN');
INSERT INTO role (id, description, name) VALUES (2, 'Engenheiro', 'ENG');
INSERT INTO role (id, description, name) VALUES (3, 'Técnico', 'TEC');
INSERT INTO role (id, description, name) VALUES (4, 'Consultor', 'CON');

INSERT INTO `user` ( `fullname`, `password`, `username`) VALUES ( 'Administrador', '$2a$10$KwwdXuqnD70.mXfLuJ6LJu/F.yIYYNsAXo6v0dWDCDLu3v0w6r2ea',  'admin');