package tcc.sica.backendadmin.model;

import java.util.List;

public class AuthToken {

    private String token;
    private String username;
    private String fullname;
    private List<Role> roles;

    public AuthToken(){

    }

    public AuthToken(String token){
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public AuthToken(String token, String username, String fullname, List<Role> roles) {
        this.token = token;
        this.username = username;
        this.fullname = fullname;
        this.roles = roles;
    }

}