package tcc.sica.backendadmin.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import tcc.sica.backendadmin.model.Role;

public interface RoleRepository extends CrudRepository<Role, Long> {

    @Query("SELECT r FROM Role r WHERE r.id = :id")
    Role findOneRole(Long id);
}