package tcc.sica.backendadmin.service;

import java.util.List;

import tcc.sica.backendadmin.model.User;
import tcc.sica.backendadmin.model.UserDTO;

public interface UserService {

    User save(UserDTO user);
    List<User> findAll();
    void delete(long id);
    User findOne(String username);

    User findById(Long id);

    User update(User user);
}