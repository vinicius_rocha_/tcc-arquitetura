package tcc.sica.backendadmin.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import tcc.sica.backendadmin.model.Role;
import tcc.sica.backendadmin.model.User;
import tcc.sica.backendadmin.model.UserDTO;
import tcc.sica.backendadmin.repository.RoleRepository;
import tcc.sica.backendadmin.repository.UserRepository;
import tcc.sica.backendadmin.service.UserService;

import java.util.*;


@Service(value = "userService")
public class UserServiceImpl implements UserDetailsService, UserService {
	
	@Autowired
	private UserRepository userRep;

	@Autowired RoleRepository roleRep;

	@Autowired
	private BCryptPasswordEncoder bcryptEncoder;

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRep.findByUsername(username);
		if(user == null){
			throw new UsernameNotFoundException("Invalid username or password.");
		}
		return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), getAuthority(user));
	}

	private Set<SimpleGrantedAuthority> getAuthority(User user) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
		user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getName()));
		});
		return authorities;
		//return Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"));
	}

	public List<User> findAll() {
		List<User> list = new ArrayList<>();
		userRep.findAll().iterator().forEachRemaining(list::add);
		return list;
	}

	@Override
	public void delete(long id) {
		userRep.deleteById(id);
	}

	@Override
	public User findOne(String username) {
		return userRep.findByUsername(username);
	}

	@Override
	public User findById(Long id) {
		return userRep.findById(id).get();
	}

	@Override
    public User save(UserDTO user) {
		User newUser = new User();
	    newUser.setUsername(user.getUsername());
        newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
		newUser.setFullname(user.getFullname());


		List<Role> roles = new ArrayList<Role>();
 		for(Role entry : user.getRoles()) {
			 roles.add(roleRep.findOneRole(entry.getId()));
		 }
		newUser.setRoles(roles);


        return userRep.save(newUser);
    }

	@Override
	public User update(User user) {
		User foundUser = null;
		if( (foundUser = this.findById(user.getId())) != null) {
			if(user.getPassword() != null)
				user.setPassword(bcryptEncoder.encode(user.getPassword()));
			else
				user.setPassword(foundUser.getPassword());

			List<Role> roles = new ArrayList<Role>();
			for(Role entry : user.getRoles()) {
				roles.add(roleRep.findOneRole(entry.getId()));
			}
			user.setRoles(roles);
		}
		
		return userRep.save(user);
	}
}