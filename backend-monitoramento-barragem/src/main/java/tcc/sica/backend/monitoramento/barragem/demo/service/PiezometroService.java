package tcc.sica.backend.monitoramento.barragem.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tcc.sica.backend.monitoramento.barragem.demo.entity.Piezometro;
import tcc.sica.backend.monitoramento.barragem.demo.repository.PiezometroRepository;

@Service
public class PiezometroService {

    @Autowired
    private PiezometroRepository piezometroRepository;

    public List<Piezometro> findByIdBarragem(Long id) {
        return piezometroRepository.findByIdBarragem(id);
    }

    public Piezometro save(Piezometro entity) {
        return piezometroRepository.save(entity);
    }
}