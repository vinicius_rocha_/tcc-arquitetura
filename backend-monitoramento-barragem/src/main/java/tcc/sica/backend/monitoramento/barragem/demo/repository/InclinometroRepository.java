package tcc.sica.backend.monitoramento.barragem.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import tcc.sica.backend.monitoramento.barragem.demo.entity.Inclinometro;

public interface InclinometroRepository extends JpaRepository<Inclinometro, Long> {

    @Query("SELECT i FROM Inclinometro i INNER JOIN i.barragem b ON i.barragem.id = b.id WHERE b.id = :id")
    List<Inclinometro> findByIdBarragem(@Param("id") Long id);
    
} 