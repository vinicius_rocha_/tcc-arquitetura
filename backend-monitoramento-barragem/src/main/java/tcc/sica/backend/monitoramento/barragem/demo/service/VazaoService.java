package tcc.sica.backend.monitoramento.barragem.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tcc.sica.backend.monitoramento.barragem.demo.entity.Vazao;
import tcc.sica.backend.monitoramento.barragem.demo.repository.VazaoRepository;

@Service
public class VazaoService {

    @Autowired
    private VazaoRepository vazaoRepository;

    public List<Vazao> findByIdBarragem(Long id) {
        return vazaoRepository.findByIdBarragem(id);
    }

    public Vazao save(Vazao vazao) {
        return vazaoRepository.save(vazao);
    }
}