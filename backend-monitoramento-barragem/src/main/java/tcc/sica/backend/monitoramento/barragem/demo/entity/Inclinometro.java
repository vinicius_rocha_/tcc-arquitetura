package tcc.sica.backend.monitoramento.barragem.demo.entity;

import java.util.List;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Inclinometro {
    
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

    private Date data;

    @OneToMany(targetEntity = Profundidade.class)
    private List profundidade;

    @OneToMany(targetEntity = Deslocamento.class)
    private List deslocamento;

    @ManyToOne
    private Barragem barragem;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public List<Long> getProfundidade() {
        return profundidade;
    }

    public void setProfundidade(List<Long> profundidade) {
        this.profundidade = profundidade;
    }

    public List<Long> getDeslocamento() {
        return deslocamento;
    }

    public void setDeslocamento(List<Long> deslocamento) {
        this.deslocamento = deslocamento;
    }

    public Barragem getBarragem() {
        return barragem;
    }

    public void setBarragem(Barragem barragem) {
        this.barragem = barragem;
    }
}