package tcc.sica.backend.monitoramento.barragem.demo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Barragem {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

    private String nome;

    private String municipio;

    private String estado;

    private String Bacia;

    private String latitude;

    private String longitude;

    private String nivelAlertaVazao;

    private String nivelAlertaPiezometro;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getBacia() {
        return Bacia;
    }

    public void setBacia(String bacia) {
        Bacia = bacia;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getNivelAlertaVazao() {
        return nivelAlertaVazao;
    }

    public void setNivelAlertaVazao(String nivelAlertaVazao) {
        this.nivelAlertaVazao = nivelAlertaVazao;
    }

    public String getNivelAlertaPiezometro() {
        return nivelAlertaPiezometro;
    }

    public void setNivelAlertaPiezometro(String nivelAlertaPiezometro) {
        this.nivelAlertaPiezometro = nivelAlertaPiezometro;
    }
}