package tcc.sica.backend.monitoramento.barragem.demo.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Piezometro {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

    private Date data;

    private Long kpa;

    @ManyToOne
    @JsonIgnore
    private Barragem barragem;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Long getKpa() {
        return kpa;
    }

    public void setKpa(Long kpa) {
        this.kpa = kpa;
    }

    public Barragem getBarragem() {
        return barragem;
    }

    public void setBarragem(Barragem barragem) {
        this.barragem = barragem;
    }
}