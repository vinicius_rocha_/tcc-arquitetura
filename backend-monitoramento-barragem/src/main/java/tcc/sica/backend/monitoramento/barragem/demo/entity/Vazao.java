package tcc.sica.backend.monitoramento.barragem.demo.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Vazao {
    
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

    private Date data;

    private Long metrosCubicos;

    @ManyToOne
    @JsonIgnore
    private Barragem barragem;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Long getMetrosCubicos() {
        return metrosCubicos;
    }

    public void setMetrosCubicos(Long metrosCubicos) {
        this.metrosCubicos = metrosCubicos;
    }

    public Barragem getBarragem() {
        return barragem;
    }

    public void setBarragem(Barragem barragem) {
        this.barragem = barragem;
    }
}