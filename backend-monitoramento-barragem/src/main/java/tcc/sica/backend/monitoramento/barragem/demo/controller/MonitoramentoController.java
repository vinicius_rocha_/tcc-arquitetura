package tcc.sica.backend.monitoramento.barragem.demo.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import tcc.sica.backend.monitoramento.barragem.demo.entity.Barragem;
import tcc.sica.backend.monitoramento.barragem.demo.entity.Inclinometro;
import tcc.sica.backend.monitoramento.barragem.demo.entity.Piezometro;
import tcc.sica.backend.monitoramento.barragem.demo.entity.Vazao;
import tcc.sica.backend.monitoramento.barragem.demo.service.BarragemService;
import tcc.sica.backend.monitoramento.barragem.demo.service.InclinometroService;
import tcc.sica.backend.monitoramento.barragem.demo.service.PiezometroService;
import tcc.sica.backend.monitoramento.barragem.demo.service.VazaoService;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class MonitoramentoController {

    @Autowired
    private BarragemService barragemService;

    @Autowired
    private InclinometroService inclinometroService;

    @Autowired
    private PiezometroService piezometroService;

    @Autowired
    private VazaoService vazaoService;

    @GetMapping(value = "/barragem")
    public ResponseEntity<List<Barragem>> getAllBarragem() {
        return ResponseEntity.ok(barragemService.findAll());
    }

    @GetMapping(value = "/barragem/{id}")
    public ResponseEntity<Barragem> getBarragemById(@PathVariable Long id) {
        Optional<Barragem> barragem = barragemService.findById(id);
        if (!barragem.isPresent()) {
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(barragem.get());
    }

    @GetMapping(value = "/barragem/{id}/inclinometro")
    public ResponseEntity<List<Inclinometro>> getInclinometro(@PathVariable Long id) {
        Optional<Barragem> barragem = barragemService.findById(id);
        if (!barragem.isPresent()) {
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(inclinometroService.findByIdBarragem(id));
    }

    @GetMapping(value = "/barragem/{id}/piezometro")
    public ResponseEntity<List<Piezometro>> getPiezometro(@PathVariable Long id) {
        Optional<Barragem> barragem = barragemService.findById(id);
        if (!barragem.isPresent()) {
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(piezometroService.findByIdBarragem(id));
    }

    @GetMapping(value = "/barragem/{id}/vazao")
    public ResponseEntity<List<Vazao>> getVazao(@PathVariable Long id) {
        Optional<Barragem> barragem = barragemService.findById(id);
        if (!barragem.isPresent()) {
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(vazaoService.findByIdBarragem(id));
    }

    @PostMapping(value = "/barragem/{id}/vazao")
    public ResponseEntity<Vazao> gravaVazao(@PathVariable Long id, @RequestBody final Vazao vazao)
            throws URISyntaxException {
        Optional<Barragem> barragem = barragemService.findById(id);
        if (!barragem.isPresent()) {
            ResponseEntity.badRequest().build();
        }

        validaNivel(barragem.get().getNivelAlertaVazao(), vazao.getMetrosCubicos(), barragem.get().getId());
        System.out.println("[gravaVazao] :: Alerta enviado!!");

        return ResponseEntity.ok(vazaoService.save(vazao));
    }

    @PostMapping(value = "/barragem/{id}/piezometro")
    public ResponseEntity<Piezometro> gravaPiezometro(@PathVariable Long id, @RequestBody final Piezometro piezometro)
            throws URISyntaxException {
        Optional<Barragem> barragem = barragemService.findById(id);
        if (!barragem.isPresent()) {
            ResponseEntity.badRequest().build();
        }

        validaNivel(barragem.get().getNivelAlertaPiezometro(), piezometro.getKpa(), barragem.get().getId());
        System.out.println("[gravaPiezometro] :: Alerta enviado!!");

        return ResponseEntity.ok(piezometroService.save(piezometro));
    }

    @PostMapping(value = "/barragem/{id}/alerta")
    public ResponseEntity<Optional<Barragem>> alerta(@PathVariable Long id) throws URISyntaxException {
        Optional<Barragem> barragem = barragemService.findById(id);
        if (!barragem.isPresent()) {
            ResponseEntity.badRequest().build();
        }

        if(acionarSegurancaComunicacao(barragem.get().getId()) != null) {
            System.out.println("[alerta] :: Alerta Enviado!!");
            return ResponseEntity.ok(barragem);
        } else {
            return ResponseEntity.badRequest().build();
        }
        
    }

    @PostMapping(value = "/barragem/{id}")
    public ResponseEntity<Barragem> update(@PathVariable final Long id, @RequestBody final Barragem barragemBody) {
        Optional<Barragem> barragem = barragemService.findById(id);
        if (!barragem.isPresent()) {
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(barragemService.update(barragemBody));
    }

    private void validaNivel(String nivelAlerta, Long dadoSensor, Long idBarragem) throws URISyntaxException {

        // padrao nivel de alerta <sinal>,<valor>
        String[] padrao = nivelAlerta.split(",");
        String sinal = padrao[0];
        Long valor = Long.parseLong(padrao[1]);

        boolean asc = false;

        switch (sinal) {
            case ">":
                asc = (dadoSensor > valor);
                break;
            case ">=":
                asc = (dadoSensor >= valor);
                break;
            case "<":
                asc = (dadoSensor < valor);
                break;
            case "<=":
                asc = (dadoSensor <= valor);
                break;
            case "=":
                asc = (dadoSensor == valor);
                break;
            default:
                break;
        }

        if (asc) {
            acionarSegurancaComunicacao(idBarragem);
        }
    }

    private ResponseEntity<String> acionarSegurancaComunicacao(Long barragemId) throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();
        final String baseUrl = "http://localhost:8081/comunicacao/barragem/" + barragemId;
        URI uri = new URI(baseUrl);
        return restTemplate.postForEntity(uri, null, String.class);
    }
}