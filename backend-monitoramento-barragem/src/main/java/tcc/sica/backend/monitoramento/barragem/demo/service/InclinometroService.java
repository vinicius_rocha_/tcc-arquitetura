package tcc.sica.backend.monitoramento.barragem.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tcc.sica.backend.monitoramento.barragem.demo.entity.Inclinometro;
import tcc.sica.backend.monitoramento.barragem.demo.repository.InclinometroRepository;

@Service
public class InclinometroService {

    @Autowired
    private InclinometroRepository inclinometroRepository;

    public List<Inclinometro> findByIdBarragem(Long id) {
        return inclinometroRepository.findByIdBarragem(id);
    }

}