package tcc.sica.backend.monitoramento.barragem.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import tcc.sica.backend.monitoramento.barragem.demo.entity.Piezometro;

public interface PiezometroRepository extends JpaRepository<Piezometro, Long> {

    @Query("SELECT p FROM Piezometro p INNER JOIN p.barragem b ON p.barragem.id = b.id WHERE b.id = :id")
    List<Piezometro> findByIdBarragem(@Param("id") Long id);
    
} 