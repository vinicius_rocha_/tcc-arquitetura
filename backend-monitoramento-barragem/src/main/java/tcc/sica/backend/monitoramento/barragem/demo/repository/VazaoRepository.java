package tcc.sica.backend.monitoramento.barragem.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import tcc.sica.backend.monitoramento.barragem.demo.entity.Vazao;

public interface VazaoRepository extends JpaRepository<Vazao, Long> {

    @Query("SELECT v FROM Vazao v INNER JOIN v.barragem b ON v.barragem.id = b.id WHERE b.id = :id")
    List<Vazao> findByIdBarragem(@Param("id") Long id);
    
} 