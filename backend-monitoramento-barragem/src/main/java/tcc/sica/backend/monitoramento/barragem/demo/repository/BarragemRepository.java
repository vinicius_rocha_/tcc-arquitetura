package tcc.sica.backend.monitoramento.barragem.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import tcc.sica.backend.monitoramento.barragem.demo.entity.Barragem;

public interface BarragemRepository extends JpaRepository<Barragem, Long> {
    
} 