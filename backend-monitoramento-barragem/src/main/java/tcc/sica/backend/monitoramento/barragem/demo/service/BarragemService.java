package tcc.sica.backend.monitoramento.barragem.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tcc.sica.backend.monitoramento.barragem.demo.entity.Barragem;
import tcc.sica.backend.monitoramento.barragem.demo.repository.BarragemRepository;

@Service
public class BarragemService {

    @Autowired
    private BarragemRepository barragemRepository;

    public List<Barragem> findAll() {
        return barragemRepository.findAll();
    }

    public Optional<Barragem> findById(Long id) {
        return barragemRepository.findById(id);
    }

    public Barragem update(Barragem barragem) {
        return barragemRepository.save(barragem);
    }
}