-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- CREATE DATABASE "admin-db" ------------------------------
CREATE DATABASE IF NOT EXISTS `admin-db` CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `admin-db`;
-- ---------------------------------------------------------


-- CREATE TABLE "role" -----------------------------------------
CREATE TABLE `role` ( 
	`id` BigInt( 0 ) AUTO_INCREMENT NOT NULL,
	`description` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	`name` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------


-- CREATE TABLE "user" -----------------------------------------
CREATE TABLE `user` ( 
	`id` BigInt( 0 ) AUTO_INCREMENT NOT NULL,
	`fullname` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	`password` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	`username` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------


-- CREATE TABLE "user_roles" -----------------------------------
CREATE TABLE `user_roles` ( 
	`user_id` BigInt( 0 ) NOT NULL,
	`role_id` BigInt( 0 ) NOT NULL )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------


-- Dump data of "role" -------------------------------------
INSERT INTO `role`(`id`,`description`,`name`) VALUES 
( '1', 'Administrador', 'ADMIN' ),
( '2', 'Técnico', 'TEC' ),
( '3', 'Engenheiro', 'ENG' ),
( '4', 'Consultor', 'CON' );
-- ---------------------------------------------------------


-- Dump data of "user" -------------------------------------
INSERT INTO `user`(`id`,`fullname`,`password`,`username`) VALUES 
( '1', 'Administrador', '$2a$10$KwwdXuqnD70.mXfLuJ6LJu/F.yIYYNsAXo6v0dWDCDLu3v0w6r2ea', 'admin' );
-- ---------------------------------------------------------


-- Dump data of "user_roles" -------------------------------
INSERT INTO `user_roles`(`user_id`,`role_id`) VALUES 
( '1', '1' );
-- ---------------------------------------------------------


-- CREATE INDEX "FK55itppkw3i07do3h7qoclqd4k" ------------------
CREATE INDEX `FK55itppkw3i07do3h7qoclqd4k` USING BTREE ON `user_roles`( `user_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "FKrhfovtciq1l558cw6udg0h0d3" ------------------
CREATE INDEX `FKrhfovtciq1l558cw6udg0h0d3` USING BTREE ON `user_roles`( `role_id` );
-- -------------------------------------------------------------


-- CREATE LINK "FKrhfovtciq1l558cw6udg0h0d3" -------------------
ALTER TABLE `user_roles`
	ADD CONSTRAINT `FKrhfovtciq1l558cw6udg0h0d3` FOREIGN KEY ( `role_id` )
	REFERENCES `role`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;
-- -------------------------------------------------------------


-- CREATE LINK "FK55itppkw3i07do3h7qoclqd4k" -------------------
ALTER TABLE `user_roles`
	ADD CONSTRAINT `FK55itppkw3i07do3h7qoclqd4k` FOREIGN KEY ( `user_id` )
	REFERENCES `user`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;
-- -------------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


