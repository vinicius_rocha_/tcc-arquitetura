package tcc.sica.sicabackendativos.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import tcc.sica.sicabackendativos.entity.Manutencao;
import tcc.sica.sicabackendativos.repository.ManutencaoRepository;

@Service
public class ManutencaoService {
    
    private final ManutencaoRepository mp;

    public ManutencaoService(ManutencaoRepository mp) {
        this.mp = mp;
    }

    public List<Manutencao> findAll() {
        return mp.findAll();
    }

    public List<Manutencao> findAllByAtivoId(Long id) {
        return mp.findAllByAtivoId(id);
    }

    public Optional<Manutencao> findById(Long id) {
        return mp.findById(id);
    }

    public Manutencao save(Manutencao stock) {
        return mp.save(stock);
    }

    public void deleteById(Long id) {
        mp.deleteById(id);
    }
}