package tcc.sica.sicabackendativos.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import tcc.sica.sicabackendativos.entity.Cronograma;
import tcc.sica.sicabackendativos.repository.CronogramaRepository;

@Service
public class CronogramaService {
    
    private final CronogramaRepository cp;

    public CronogramaService(CronogramaRepository cp) {
        this.cp = cp;
    }

    public List<Cronograma> findAll() {
        return cp.findAll();
    }

    public List<Cronograma> findAllByAtivoId(Long id) {
        return cp.findAllByAtivoId(id);
    }

    public Optional<Cronograma> findById(Long id) {
        return cp.findById(id);
    }

    public Cronograma save(Cronograma stock) {
        return cp.save(stock);
    }

    public void deleteById(Long id) {
        cp.deleteById(id);
    }
}