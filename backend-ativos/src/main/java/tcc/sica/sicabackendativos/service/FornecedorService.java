package tcc.sica.sicabackendativos.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tcc.sica.sicabackendativos.entity.Fornecedor;
import tcc.sica.sicabackendativos.repository.FornecedorRepository;

@Service
public class FornecedorService {
    
    @Autowired
    private FornecedorRepository fp;

    public List<Fornecedor> findAll() {
        return fp.findAll();
    }    
    
    public Optional<Fornecedor> findById(Long id) {
        return fp.findById(id);
    }

    public Fornecedor save(Fornecedor fornecedor) {
        return fp.save(fornecedor);
    }

    public void delete(Long id) {
        fp.deleteById(id);
    }
}