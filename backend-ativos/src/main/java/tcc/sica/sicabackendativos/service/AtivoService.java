package tcc.sica.sicabackendativos.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tcc.sica.sicabackendativos.entity.Ativo;
import tcc.sica.sicabackendativos.repository.AtivoRepository;
@Service
public class AtivoService {
   
    @Autowired
    private AtivoRepository ap;

    public List<Ativo> findAll() {
        return ap.findAll();
    }

    public Optional<Ativo> findById(Long id) {
        return ap.findById(id);
    }

    public Ativo save(Ativo stock) {
        return ap.save(stock);
    }

    public void deleteById(Long id) {
        ap.deleteById(id);
    }
}