package tcc.sica.sicabackendativos.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tcc.sica.sicabackendativos.entity.AtivoTipo;
import tcc.sica.sicabackendativos.repository.AtivoTipoRepository;

@Service
public class AtivoTipoService {

    @Autowired
    private AtivoTipoRepository ar;

    public List<AtivoTipo> findAll() {
        return ar.findAll();
    }

    public Optional<AtivoTipo> findOne(Long id) {
        return ar.findById(id);
    }

    public AtivoTipo save(AtivoTipo ativoTipo) {
        return ar.save(ativoTipo);
    }

    public void delete(Long id) {
        ar.deleteById(id);
    }
}