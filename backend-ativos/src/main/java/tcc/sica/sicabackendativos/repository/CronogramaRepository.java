package tcc.sica.sicabackendativos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import tcc.sica.sicabackendativos.entity.Cronograma;

public interface CronogramaRepository extends JpaRepository<Cronograma, Long> {

    @Query("SELECT c FROM Cronograma c INNER JOIN c.ativo a ON c.ativo.id = a.id WHERE a.id = :id")
    List<Cronograma> findAllByAtivoId(@Param("id") Long id);

}