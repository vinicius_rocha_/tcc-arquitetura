package tcc.sica.sicabackendativos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import tcc.sica.sicabackendativos.entity.AtivoTipo;

public interface AtivoTipoRepository extends JpaRepository<AtivoTipo, Long> {

}