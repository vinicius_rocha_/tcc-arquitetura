package tcc.sica.sicabackendativos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import tcc.sica.sicabackendativos.entity.Ativo;

public interface AtivoRepository extends JpaRepository<Ativo, Long> {
    
} 