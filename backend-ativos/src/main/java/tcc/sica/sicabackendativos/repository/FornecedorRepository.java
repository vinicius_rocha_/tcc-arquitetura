package tcc.sica.sicabackendativos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import tcc.sica.sicabackendativos.entity.Fornecedor;

public interface FornecedorRepository extends JpaRepository<Fornecedor, Long> {
    
} 