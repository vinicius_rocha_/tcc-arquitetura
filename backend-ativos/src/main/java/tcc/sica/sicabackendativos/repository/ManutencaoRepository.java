package tcc.sica.sicabackendativos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import tcc.sica.sicabackendativos.entity.Manutencao;

public interface ManutencaoRepository extends JpaRepository<Manutencao, Long> {

    @Query("SELECT m FROM Manutencao m INNER JOIN m.ativo a ON m.ativo.id = a.id WHERE a.id = :id")
    List<Manutencao> findAllByAtivoId(@Param("id") Long id);
    
}