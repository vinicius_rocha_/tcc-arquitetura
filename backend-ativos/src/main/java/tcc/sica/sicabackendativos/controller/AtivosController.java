package tcc.sica.sicabackendativos.controller;

import org.springframework.web.bind.annotation.RestController;

import tcc.sica.sicabackendativos.entity.Ativo;
import tcc.sica.sicabackendativos.entity.AtivoTipo;
import tcc.sica.sicabackendativos.entity.Fornecedor;
import tcc.sica.sicabackendativos.service.AtivoService;
import tcc.sica.sicabackendativos.service.AtivoTipoService;
import tcc.sica.sicabackendativos.service.FornecedorService;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class AtivosController {

    @Autowired
    private AtivoService as;

    @Autowired
    private FornecedorService fs;

    @Autowired
    private AtivoTipoService ats;

    @GetMapping
    public ResponseEntity<List<Ativo>> findAll() {
        return ResponseEntity.ok(as.findAll());
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Ativo> getById(@PathVariable final Long id) {
        final Optional<Ativo> stock = as.findById(id);
        if (!stock.isPresent()) {
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(stock.get());
    }
    
    @PostMapping
    public ResponseEntity<Ativo> create(@RequestBody final Ativo ativo) {
        return ResponseEntity.ok(as.save(ativo));
    }

    @PutMapping("/{id}")
    public ResponseEntity<Ativo> update(@PathVariable final Long id, @RequestBody final Ativo ativo) {
        if(!as.findById(id).isPresent()) {
            return ResponseEntity.badRequest().build();
        }
        ativo.setId(id);
        return ResponseEntity.ok(as.save(ativo));
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Object> delete(@PathVariable final Long id) {
        if (!as.findById(id).isPresent()) {
            return ResponseEntity.badRequest().build();
        }

        as.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/fornecedor")
    public ResponseEntity<List<Fornecedor>> findFornecedor() {
        return ResponseEntity.ok(fs.findAll());
    }

    @GetMapping(value = "/fornecedor/{id}")
    public ResponseEntity<Fornecedor> getForncedorById(@PathVariable final Long id) {
        final Optional<Fornecedor> stock = fs.findById(id);
        if (!stock.isPresent()) {
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(stock.get());
    }

    @PostMapping(value = "/fornecedor")
    public ResponseEntity<Fornecedor> saveFornecedor(@RequestBody final Fornecedor fornecedor) {
        return ResponseEntity.ok(fs.save(fornecedor));
    }

    @PutMapping(value = "/fornecedor")
    public ResponseEntity<Fornecedor> updateFornecedor(@RequestBody final Fornecedor fornecedor) {
        if(!fs.findById(fornecedor.getId()).isPresent()) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(fs.save(fornecedor));
    }

    @DeleteMapping(value = "/fornecedor/{id}")
    public ResponseEntity<Object> deleteFornecedor(@PathVariable final Long id) {
        if (!fs.findById(id).isPresent()) {
            return ResponseEntity.badRequest().build();
        }

        fs.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/tipos")
    public ResponseEntity<List<AtivoTipo>> getTipos() {
        return ResponseEntity.ok(ats.findAll());
    }
    
    @GetMapping(value = "/tipos/{id}")
    public ResponseEntity<AtivoTipo> getTipoById(@PathVariable final Long id) {
        final Optional<AtivoTipo> stock = ats.findOne(id);
        if (!stock.isPresent()) {
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(stock.get());
    }

    @PostMapping(value = "/tipos")
    public ResponseEntity<AtivoTipo> saveAtivoTipo(@RequestBody final AtivoTipo fornecedor) {
        return ResponseEntity.ok(ats.save(fornecedor));
    }

    @PutMapping(value = "/tipos")
    public ResponseEntity<AtivoTipo> updateAtivoTipo(@RequestBody final AtivoTipo fornecedor) {
        if(!ats.findOne(fornecedor.getId()).isPresent()) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(ats.save(fornecedor));
    }

    @DeleteMapping(value = "/tipos/{id}")
    public ResponseEntity<Object> deleteAtivoTipo(@PathVariable final Long id) {
        if (!ats.findOne(id).isPresent()) {
            return ResponseEntity.badRequest().build();
        }

        ats.delete(id);
        return ResponseEntity.ok().build();
    }
}