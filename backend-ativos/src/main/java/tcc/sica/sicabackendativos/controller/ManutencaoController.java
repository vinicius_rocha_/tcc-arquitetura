package tcc.sica.sicabackendativos.controller;

import org.springframework.web.bind.annotation.RestController;

import tcc.sica.sicabackendativos.entity.Manutencao;
import tcc.sica.sicabackendativos.service.ManutencaoService;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class ManutencaoController {

    private final ManutencaoService ms;

    public ManutencaoController(ManutencaoService ms) {
        this.ms = ms;
    }

    @GetMapping("/manutencao")
    public ResponseEntity<List<Manutencao>> findAll() {
        return ResponseEntity.ok(ms.findAll());
    }

    @GetMapping(value = "/manutencao/{id}")
    public ResponseEntity<Manutencao> getById(@PathVariable Long id) {
        Optional<Manutencao> stock = ms.findById(id);
        if (!stock.isPresent()) {
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(stock.get());
    }

    @GetMapping(value = "/manutencao/ativo/{id}")
    public ResponseEntity<List<Manutencao>> getByAtivo(@PathVariable Long id) {
        
        return ResponseEntity.ok(ms.findAllByAtivoId(id));
    }
    
    @PostMapping("/manutencao")
    public ResponseEntity<Manutencao> create(@RequestBody Manutencao manutencao) {
        return ResponseEntity.ok(ms.save(manutencao));
    }

    @PutMapping("/manutencao/{id}")
    public ResponseEntity<Manutencao> update(@PathVariable Long id, @RequestBody Manutencao manutencao) {
        if(!ms.findById(id).isPresent()) {
            return ResponseEntity.badRequest().build();
        }
        manutencao.setId(id);
        return ResponseEntity.ok(ms.save(manutencao));
    }

    @DeleteMapping(value = "/manutencao/{id}")
    public ResponseEntity<Object> delete(@PathVariable Long id) {
        if (!ms.findById(id).isPresent()) {
            return ResponseEntity.badRequest().build();
        }

        ms.deleteById(id);
        return ResponseEntity.ok().build();
    }
}