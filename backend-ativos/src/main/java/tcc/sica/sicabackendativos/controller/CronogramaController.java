package tcc.sica.sicabackendativos.controller;

import org.springframework.web.bind.annotation.RestController;

import tcc.sica.sicabackendativos.entity.Cronograma;
import tcc.sica.sicabackendativos.service.CronogramaService;

import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class CronogramaController {

    private final CronogramaService cs;

    public CronogramaController(CronogramaService cs) {
        this.cs = cs;
    }

    @GetMapping("/cronograma")
    public ResponseEntity<List<Cronograma>> findAll() {
        return ResponseEntity.ok(cs.findAll());
    }

    @GetMapping(value = "/cronograma/{id}")
    public ResponseEntity<Cronograma> getById(@PathVariable Long id) {
        Optional<Cronograma> stock = cs.findById(id);
        if (!stock.isPresent()) {
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(stock.get());
    }

    @GetMapping(value = "/cronograma/ativo/{id}")
    public ResponseEntity<List<Cronograma>> getByAtivo(@PathVariable Long id) {
        
        return ResponseEntity.ok(cs.findAllByAtivoId(id));
    }
    
    @PostMapping("/cronograma")
    public ResponseEntity<Cronograma> create(@RequestBody Cronograma cronograma) {
        return ResponseEntity.ok(cs.save(cronograma));
    }

    @PutMapping("/cronograma/{id}")
    public ResponseEntity<Cronograma> update(@PathVariable Long id, @RequestBody Cronograma cronograma) {
        if(!cs.findById(id).isPresent()) {
            return ResponseEntity.badRequest().build();
        }
        cronograma.setId(id);
        System.out.println("############ ativo id = " + cronograma.getAtivo().getId());
        return ResponseEntity.ok(cs.save(cronograma));
    }

    @DeleteMapping(value = "/cronograma/{id}")
    public ResponseEntity<Object> delete(@PathVariable Long id) {
        if (!cs.findById(id).isPresent()) {
            return ResponseEntity.badRequest().build();
        }

        cs.deleteById(id);
        return ResponseEntity.ok().build();
    }
}