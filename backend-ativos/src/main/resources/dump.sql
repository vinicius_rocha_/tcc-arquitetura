-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- Dump data of "ativo" ------------------------------------
INSERT INTO `ativo`(`id`,`data_aquisicao`,`descricao`,`nota_fiscal`,`fornecedor_id`,`tipo_id`) VALUES 
( '4', '2017-01-01 00:00:00.000000', 'Ativo Quatro atualizado', 'nota fiscal', '1', '2' ),
( '5', '2020-01-01 00:00:00.000000', 'Ativo Um', 'nota fiscal', '1', '2' ),
( '6', '2020-01-01 00:00:00.000000', 'Ativo Dois', 'nota fiscal', '1', '1' ),
( '7', '2016-09-23 00:00:00.000000', 'ativo pelo angular', 'nota', '1', '1' ),
( '12', '2016-04-17 00:00:00.000000', 'ativo pelo angular 2', 'nota', '1', '1' ),
( '13', '2016-09-23 00:00:00.000000', 'ativo pelo angular 3 atua', 'nota', '1', '1' ),
( '14', '2020-01-01 00:00:00.000000', 'Ativo teste', 'link.com.br/notafiscal', '1', '1' );
-- ---------------------------------------------------------


-- Dump data of "ativo_tipo" -------------------------------
INSERT INTO `ativo_tipo`(`id`,`descricao`) VALUES 
( '1', 'Tipo Um' ),
( '2', 'Tipo Dois atualizado' );
-- ---------------------------------------------------------


-- Dump data of "cronograma" -------------------------------
INSERT INTO `cronograma`(`id`,`observacoes`,`proxima_manutencao`,`tipo_manutencao`,`ativo_id`) VALUES 
( '5', 'atualizado', '2020-03-04 00:00:00.000000', 'PREVENTIVA', '4' ),
( '8', 'fazer direito', '2020-04-20 00:00:00.000000', 'PREVENTIVA', '4' );
-- ---------------------------------------------------------


-- Dump data of "fornecedor" -------------------------------
INSERT INTO `fornecedor`(`id`,`cnpj`,`email`,`endereco`,`nome_fantasia`,`telefone1`,`telefone2`,`telefone3`) VALUES 
( '1', '11111111111', 'f1@email.com', 'endereco fornecedor um', 'Fornecedor Um', '12345678', NULL, NULL ),
( '7', '10293847561', 'f2@sica.com.br', 'Rua teste, 123 - 03212-000 São Paulo - SP', 'Fornecedor Dois', '18273645', '', '' );
-- ---------------------------------------------------------


-- Dump data of "manutencao" -------------------------------
INSERT INTO `manutencao`(`id`,`data_realizada`,`observacoes`,`tipo_manutencao`,`usuario`,`ativo_id`) VALUES 
( '4', '2020-03-02', 'atualizado denovo', 'PREVENTIVA', 'vinicius', '4' ),
( '5', '2020-04-03', 'atualizado denovo', 'PREVENTIVA', 'vinicius', '4' ),
( '7', '2020-03-23', 'tcc', 'CORRETIVA', '1', '4' );
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


