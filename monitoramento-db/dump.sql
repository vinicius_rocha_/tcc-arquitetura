-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- CREATE DATABASE "monitoramento-db" ----------------------
CREATE DATABASE IF NOT EXISTS `monitoramento-db` CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `monitoramento-db`;
-- ---------------------------------------------------------


-- CREATE TABLE "barragem" -------------------------------------
CREATE TABLE `barragem` ( 
	`id` BigInt( 0 ) AUTO_INCREMENT NOT NULL,
	`bacia` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	`estado` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	`latitude` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	`longitude` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	`municipio` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	`nivel_alerta_piezometro` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	`nivel_alerta_vazao` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	`nome` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------


-- CREATE TABLE "deslocamento" ---------------------------------
CREATE TABLE `deslocamento` ( 
	`id` BigInt( 0 ) AUTO_INCREMENT NOT NULL,
	`leitura` BigInt( 0 ) NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------


-- CREATE TABLE "inclinometro" ---------------------------------
CREATE TABLE `inclinometro` ( 
	`id` BigInt( 0 ) AUTO_INCREMENT NOT NULL,
	`data` DateTime NULL,
	`barragem_id` BigInt( 0 ) NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "inclinometro_deslocamento" --------------------
CREATE TABLE `inclinometro_deslocamento` ( 
	`inclinometro_id` BigInt( 0 ) NOT NULL,
	`deslocamento_id` BigInt( 0 ) NOT NULL,
	CONSTRAINT `UK_rm033o5llsuh99mhgbhe00kdm` UNIQUE( `deslocamento_id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------


-- CREATE TABLE "inclinometro_profundidade" --------------------
CREATE TABLE `inclinometro_profundidade` ( 
	`inclinometro_id` BigInt( 0 ) NOT NULL,
	`profundidade_id` BigInt( 0 ) NOT NULL,
	CONSTRAINT `UK_fgl5ftp4cgga88yg90k0ngjsy` UNIQUE( `profundidade_id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------


-- CREATE TABLE "piezometro" -----------------------------------
CREATE TABLE `piezometro` ( 
	`id` BigInt( 0 ) AUTO_INCREMENT NOT NULL,
	`data` DateTime NULL,
	`kpa` BigInt( 0 ) NULL,
	`barragem_id` BigInt( 0 ) NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "profundidade" ---------------------------------
CREATE TABLE `profundidade` ( 
	`id` BigInt( 0 ) AUTO_INCREMENT NOT NULL,
	`leitura` BigInt( 0 ) NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------


-- CREATE TABLE "vazao" ----------------------------------------
CREATE TABLE `vazao` ( 
	`id` BigInt( 0 ) AUTO_INCREMENT NOT NULL,
	`data` DateTime NULL,
	`metros_cubicos` BigInt( 0 ) NULL,
	`barragem_id` BigInt( 0 ) NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- Dump data of "barragem" ---------------------------------
INSERT INTO `barragem`(`id`,`bacia`,`estado`,`latitude`,`longitude`,`municipio`,`nivel_alerta_piezometro`,`nivel_alerta_vazao`,`nome`) VALUES 
( '1', 'Bacia teste', 'Minas Gerais', '-23.0102039848', '-46.0102039848', 'Brumadinho', NULL, NULL, 'Barragem 1' ),
( '2', 'Bacia teste 2', 'Minas Gerais', '-23.674893294', '-46.5467578903', 'Brumadinho', NULL, NULL, 'Barragem 2' );
-- ---------------------------------------------------------


-- Dump data of "deslocamento" -----------------------------
INSERT INTO `deslocamento`(`id`,`leitura`) VALUES 
( '1', '320' ),
( '2', '300' ),
( '3', '260' ),
( '4', '240' ),
( '5', '200' ),
( '6', '180' ),
( '7', '140' ),
( '8', '130' ),
( '9', '110' ),
( '10', '80' ),
( '11', '40' ),
( '12', '10' ),
( '13', '9' ),
( '14', '8' ),
( '15', '7' );
-- ---------------------------------------------------------


-- Dump data of "inclinometro" -----------------------------
INSERT INTO `inclinometro`(`id`,`data`,`barragem_id`) VALUES 
( '1', '2020-03-01 00:00:00.000000', '2' ),
( '2', '2020-03-02 00:00:00.000000', '2' ),
( '3', '2020-03-03 00:00:00.000000', '2' ),
( '4', '2020-03-04 00:00:00.000000', '2' );
-- ---------------------------------------------------------


-- Dump data of "inclinometro_deslocamento" ----------------
INSERT INTO `inclinometro_deslocamento`(`inclinometro_id`,`deslocamento_id`) VALUES 
( '1', '1' ),
( '1', '2' ),
( '1', '3' ),
( '1', '4' ),
( '1', '5' ),
( '1', '6' ),
( '1', '7' ),
( '1', '8' ),
( '1', '9' ),
( '1', '10' ),
( '1', '11' ),
( '1', '12' ),
( '1', '13' ),
( '1', '14' ),
( '1', '15' );
-- ---------------------------------------------------------


-- Dump data of "inclinometro_profundidade" ----------------
INSERT INTO `inclinometro_profundidade`(`inclinometro_id`,`profundidade_id`) VALUES 
( '1', '1' ),
( '1', '2' ),
( '1', '3' ),
( '1', '4' ),
( '1', '5' ),
( '1', '6' ),
( '1', '7' ),
( '1', '8' ),
( '1', '9' ),
( '1', '10' ),
( '1', '11' ),
( '1', '12' ),
( '1', '13' ),
( '1', '14' ),
( '1', '15' );
-- ---------------------------------------------------------


-- Dump data of "piezometro" -------------------------------
INSERT INTO `piezometro`(`id`,`data`,`kpa`,`barragem_id`) VALUES 
( '1', '2020-03-01 00:00:00.000000', '7', '2' ),
( '2', '2020-03-02 00:00:00.000000', '6', '2' ),
( '3', '2020-03-03 00:00:00.000000', '5', '2' ),
( '4', '2020-03-04 00:00:00.000000', '6', '2' ),
( '5', '2020-03-05 00:00:00.000000', '2', '2' ),
( '6', '2020-03-06 00:00:00.000000', '9', '2' ),
( '7', '2020-03-07 00:00:00.000000', '5', '2' ),
( '8', '2020-03-08 00:00:00.000000', '4', '2' ),
( '9', '2020-03-09 00:00:00.000000', '7', '2' ),
( '10', '2020-03-10 00:00:00.000000', '2', '2' ),
( '11', '2020-03-11 00:00:00.000000', '5', '2' ),
( '12', '2020-03-12 00:00:00.000000', '3', '2' );
-- ---------------------------------------------------------


-- Dump data of "profundidade" -----------------------------
INSERT INTO `profundidade`(`id`,`leitura`) VALUES 
( '1', '1' ),
( '2', '2' ),
( '3', '3' ),
( '4', '4' ),
( '5', '5' ),
( '6', '6' ),
( '7', '7' ),
( '8', '8' ),
( '9', '9' ),
( '10', '10' ),
( '11', '11' ),
( '12', '12' ),
( '13', '13' ),
( '14', '14' ),
( '15', '15' );
-- ---------------------------------------------------------


-- Dump data of "vazao" ------------------------------------
INSERT INTO `vazao`(`id`,`data`,`metros_cubicos`,`barragem_id`) VALUES 
( '1', '2020-03-01 00:00:00.000000', '7', '2' ),
( '2', '2020-03-02 00:00:00.000000', '3', '2' ),
( '3', '2020-03-03 00:00:00.000000', '1', '2' ),
( '4', '2020-03-04 00:00:00.000000', '4', '2' ),
( '5', '2020-03-05 00:00:00.000000', '3', '2' ),
( '6', '2020-03-06 00:00:00.000000', '8', '2' ),
( '7', '2020-03-07 00:00:00.000000', '12', '2' ),
( '8', '2020-03-08 00:00:00.000000', '5', '2' ),
( '9', '2020-03-09 00:00:00.000000', '2', '2' ),
( '10', '2020-03-10 00:00:00.000000', '8', '2' ),
( '11', '2020-03-11 00:00:00.000000', '9', '2' ),
( '12', '2020-03-12 00:00:00.000000', '7', '2' );
-- ---------------------------------------------------------


-- CREATE INDEX "FKn2b8hk7mn3yctcx23llhvrw76" ------------------
CREATE INDEX `FKn2b8hk7mn3yctcx23llhvrw76` USING BTREE ON `inclinometro`( `barragem_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "FKmgacgby6tvynxxkwy1mjm7in5" ------------------
CREATE INDEX `FKmgacgby6tvynxxkwy1mjm7in5` USING BTREE ON `inclinometro_deslocamento`( `inclinometro_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "FK85dacc7tomqslpqmpm42aieol" ------------------
CREATE INDEX `FK85dacc7tomqslpqmpm42aieol` USING BTREE ON `inclinometro_profundidade`( `inclinometro_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "FKmylmykinkdw1is2bur144u2sn" ------------------
CREATE INDEX `FKmylmykinkdw1is2bur144u2sn` USING BTREE ON `piezometro`( `barragem_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "FKta990bqi1ehttpabcxbft96mq" ------------------
CREATE INDEX `FKta990bqi1ehttpabcxbft96mq` USING BTREE ON `vazao`( `barragem_id` );
-- -------------------------------------------------------------


-- CREATE LINK "FKn2b8hk7mn3yctcx23llhvrw76" -------------------
ALTER TABLE `inclinometro`
	ADD CONSTRAINT `FKn2b8hk7mn3yctcx23llhvrw76` FOREIGN KEY ( `barragem_id` )
	REFERENCES `barragem`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;
-- -------------------------------------------------------------


-- CREATE LINK "FK98qrxbfl5iynam1aoxbb9do9s" -------------------
ALTER TABLE `inclinometro_deslocamento`
	ADD CONSTRAINT `FK98qrxbfl5iynam1aoxbb9do9s` FOREIGN KEY ( `deslocamento_id` )
	REFERENCES `deslocamento`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;
-- -------------------------------------------------------------


-- CREATE LINK "FKmgacgby6tvynxxkwy1mjm7in5" -------------------
ALTER TABLE `inclinometro_deslocamento`
	ADD CONSTRAINT `FKmgacgby6tvynxxkwy1mjm7in5` FOREIGN KEY ( `inclinometro_id` )
	REFERENCES `inclinometro`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;
-- -------------------------------------------------------------


-- CREATE LINK "FKt38n9cr4gqyoyjgfb32aqibnr" -------------------
ALTER TABLE `inclinometro_profundidade`
	ADD CONSTRAINT `FKt38n9cr4gqyoyjgfb32aqibnr` FOREIGN KEY ( `profundidade_id` )
	REFERENCES `profundidade`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;
-- -------------------------------------------------------------


-- CREATE LINK "FK85dacc7tomqslpqmpm42aieol" -------------------
ALTER TABLE `inclinometro_profundidade`
	ADD CONSTRAINT `FK85dacc7tomqslpqmpm42aieol` FOREIGN KEY ( `inclinometro_id` )
	REFERENCES `inclinometro`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;
-- -------------------------------------------------------------


-- CREATE LINK "FKmylmykinkdw1is2bur144u2sn" -------------------
ALTER TABLE `piezometro`
	ADD CONSTRAINT `FKmylmykinkdw1is2bur144u2sn` FOREIGN KEY ( `barragem_id` )
	REFERENCES `barragem`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;
-- -------------------------------------------------------------


-- CREATE LINK "FKta990bqi1ehttpabcxbft96mq" -------------------
ALTER TABLE `vazao`
	ADD CONSTRAINT `FKta990bqi1ehttpabcxbft96mq` FOREIGN KEY ( `barragem_id` )
	REFERENCES `barragem`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;
-- -------------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


