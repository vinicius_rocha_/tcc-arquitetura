package tcc.sica.backend.segurancaecomunicacao.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class SegurancaComunicacaoController {

    @PostMapping("/barragem/{id}")
    public ResponseEntity<String> enviaAlerta(@PathVariable Long id) {
        System.out.println("[enviaAlerta] Alerta recebido e redirecionado!!");
        return ResponseEntity.ok("Alerta enviado para a barragem " + id);
    }
}