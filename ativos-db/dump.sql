-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- CREATE DATABASE "ativos-db" -----------------------------
CREATE DATABASE IF NOT EXISTS `ativos-db` CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci;
USE `ativos-db`;
-- ---------------------------------------------------------


-- CREATE TABLE "ativo" ----------------------------------------
CREATE TABLE `ativo` ( 
	`id` BigInt( 0 ) AUTO_INCREMENT NOT NULL,
	`data_aquisicao` DateTime NULL,
	`descricao` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	`nota_fiscal` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	`fornecedor_id` BigInt( 0 ) NULL,
	`tipo_id` BigInt( 0 ) NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB
AUTO_INCREMENT = 14;
-- -------------------------------------------------------------


-- CREATE TABLE "ativo_tipo" -----------------------------------
CREATE TABLE `ativo_tipo` ( 
	`id` BigInt( 0 ) AUTO_INCREMENT NOT NULL,
	`descricao` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB
AUTO_INCREMENT = 3;
-- -------------------------------------------------------------


-- CREATE TABLE "cronograma" -----------------------------------
CREATE TABLE `cronograma` ( 
	`id` BigInt( 0 ) AUTO_INCREMENT NOT NULL,
	`observacoes` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	`proxima_manutencao` DateTime NULL,
	`tipo_manutencao` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	`ativo_id` BigInt( 0 ) NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB
AUTO_INCREMENT = 8;
-- -------------------------------------------------------------


-- CREATE TABLE "fornecedor" -----------------------------------
CREATE TABLE `fornecedor` ( 
	`id` BigInt( 0 ) AUTO_INCREMENT NOT NULL,
	`cnpj` VarChar( 14 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	`email` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	`endereco` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	`nome_fantasia` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	`telefone1` VarChar( 11 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	`telefone2` VarChar( 11 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	`telefone3` VarChar( 11 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB
AUTO_INCREMENT = 7;
-- -------------------------------------------------------------


-- CREATE TABLE "manutencao" -----------------------------------
CREATE TABLE `manutencao` ( 
	`id` BigInt( 0 ) AUTO_INCREMENT NOT NULL,
	`data_realizada` Date NULL,
	`observacoes` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	`tipo_manutencao` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	`usuario` VarChar( 255 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL,
	`ativo_id` BigInt( 0 ) NULL,
	PRIMARY KEY ( `id` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci
ENGINE = InnoDB
AUTO_INCREMENT = 7;
-- -------------------------------------------------------------


-- Dump data of "ativo" ------------------------------------
INSERT INTO `ativo`(`id`,`data_aquisicao`,`descricao`,`nota_fiscal`,`fornecedor_id`,`tipo_id`) VALUES 
( '4', '2017-01-01 00:00:00.000000', 'Ativo Quatro atualizado', 'nota fiscal', '1', '2' ),
( '5', '2020-01-01 00:00:00.000000', 'Ativo Um', 'nota fiscal', '1', '2' ),
( '6', '2020-01-01 00:00:00.000000', 'Ativo Dois', 'nota fiscal', '1', '1' ),
( '7', '2016-09-23 00:00:00.000000', 'ativo pelo angular', 'nota', '1', '1' ),
( '12', '2016-04-17 00:00:00.000000', 'ativo pelo angular 2', 'nota', '1', '1' ),
( '13', '2016-09-23 00:00:00.000000', 'ativo pelo angular 3 atua', 'nota', '1', '1' ),
( '14', '2020-01-01 00:00:00.000000', 'Ativo teste', 'link.com.br/notafiscal', '1', '1' );
-- ---------------------------------------------------------


-- Dump data of "ativo_tipo" -------------------------------
INSERT INTO `ativo_tipo`(`id`,`descricao`) VALUES 
( '1', 'Tipo Um' ),
( '2', 'Tipo Dois atualizado' );
-- ---------------------------------------------------------


-- Dump data of "cronograma" -------------------------------
INSERT INTO `cronograma`(`id`,`observacoes`,`proxima_manutencao`,`tipo_manutencao`,`ativo_id`) VALUES 
( '5', 'atualizado', '2020-03-04 00:00:00.000000', 'PREVENTIVA', '4' ),
( '8', 'fazer direito', '2020-04-20 00:00:00.000000', 'PREVENTIVA', '4' );
-- ---------------------------------------------------------


-- Dump data of "fornecedor" -------------------------------
INSERT INTO `fornecedor`(`id`,`cnpj`,`email`,`endereco`,`nome_fantasia`,`telefone1`,`telefone2`,`telefone3`) VALUES 
( '1', '11111111111', 'f1@email.com', 'endereco fornecedor um', 'Fornecedor Um', '12345678', NULL, NULL ),
( '7', '10293847561', 'f2@sica.com.br', 'Rua teste, 123 - 03212-000 São Paulo - SP', 'Fornecedor Dois', '18273645', '', '' );
-- ---------------------------------------------------------


-- Dump data of "manutencao" -------------------------------
INSERT INTO `manutencao`(`id`,`data_realizada`,`observacoes`,`tipo_manutencao`,`usuario`,`ativo_id`) VALUES 
( '4', '2020-03-02', 'atualizado denovo', 'PREVENTIVA', 'vinicius', '4' ),
( '5', '2020-04-03', 'atualizado denovo', 'PREVENTIVA', 'vinicius', '4' ),
( '7', '2020-03-23', 'tcc', 'CORRETIVA', '1', '4' );
-- ---------------------------------------------------------


-- CREATE INDEX "FKcl6b6yr0c3w4r7qgrujmqkji9" ------------------
CREATE INDEX `FKcl6b6yr0c3w4r7qgrujmqkji9` USING BTREE ON `ativo`( `fornecedor_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "FKt5e75jlvp8mfw3dwvp2hlbs8" -------------------
CREATE INDEX `FKt5e75jlvp8mfw3dwvp2hlbs8` USING BTREE ON `ativo`( `tipo_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "FKrl2i4rgv61b3pjvdrn2o02p54" ------------------
CREATE INDEX `FKrl2i4rgv61b3pjvdrn2o02p54` USING BTREE ON `cronograma`( `ativo_id` );
-- -------------------------------------------------------------


-- CREATE INDEX "FK8oysma6reudv2cfw5q49s9jm6" ------------------
CREATE INDEX `FK8oysma6reudv2cfw5q49s9jm6` USING BTREE ON `manutencao`( `ativo_id` );
-- -------------------------------------------------------------


-- CREATE LINK "FK8oysma6reudv2cfw5q49s9jm6" -------------------
ALTER TABLE `manutencao`
	ADD CONSTRAINT `FK8oysma6reudv2cfw5q49s9jm6` FOREIGN KEY ( `ativo_id` )
	REFERENCES `ativo`( `id` )
	ON DELETE Cascade
	ON UPDATE No Action;
-- -------------------------------------------------------------


-- CREATE LINK "FKcl6b6yr0c3w4r7qgrujmqkji9" -------------------
ALTER TABLE `ativo`
	ADD CONSTRAINT `FKcl6b6yr0c3w4r7qgrujmqkji9` FOREIGN KEY ( `fornecedor_id` )
	REFERENCES `fornecedor`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;
-- -------------------------------------------------------------


-- CREATE LINK "FKrl2i4rgv61b3pjvdrn2o02p54" -------------------
ALTER TABLE `cronograma`
	ADD CONSTRAINT `FKrl2i4rgv61b3pjvdrn2o02p54` FOREIGN KEY ( `ativo_id` )
	REFERENCES `ativo`( `id` )
	ON DELETE Cascade
	ON UPDATE No Action;
-- -------------------------------------------------------------


-- CREATE LINK "FKt5e75jlvp8mfw3dwvp2hlbs8" --------------------
ALTER TABLE `ativo`
	ADD CONSTRAINT `FKt5e75jlvp8mfw3dwvp2hlbs8` FOREIGN KEY ( `tipo_id` )
	REFERENCES `ativo_tipo`( `id` )
	ON DELETE No Action
	ON UPDATE No Action;
-- -------------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


